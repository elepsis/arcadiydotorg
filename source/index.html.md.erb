---
layout: page_layout
pageable: true
per_page: 1
layout: page_layout
---

![arcadiy][pic]
{: #mainpic}

## Hi, I'm Arcadiy.

In July of 2016 I quit my job to travel the world with my wife. Over a year of country-hopping through Europe and Asia later, I'm coming back to the U.S. and rejoining the real world. Before that, I was a PM at Microsoft, working on OneDrive (the important bits you can't see at a glance) and then SharePoint (where I fixed at least 1% of our customers' nightmares). That's [what I'm good at][about]: understanding and then overhauling important, messy, non-obvious things for the better.

In my quest to make things better, I've learned a little bit about a lot of stuff. Here, though, I'm focused on travel, technology and careers. Want to take a look? Just the highlights, coming right up.

### How to travel around the world

Traveling for a year is more than a little different from traveling for just a few weeks on a holiday, but it turns out that most of the lessons I learned apply to every trip I take. Here's what I've figured out:

- Preparing for the trip was both exciting and exhausting. We got rid of the vast majority of our possessions, quit our jobs and figured out a bunch of logistics; here's everything we did to get ready to go. <span class="upcoming">Coming soon!</span>
- The hardest thing about planning a big trip is [figuring out the list of places to go][travel_itinerary].
- Once you decide where you're going, you have to work out what you're going to bring. We did the whole trip with carry-on backpacks (and successfully conquered all the major European low-cost carriers without having to pay). [Here's everything I brought][travel_packinglist], and [what did and didn't make the cut in the end][travel_packinglist_followup]. 
- When you're traveling on a tight budget, you have to be pretty smart about how you handle money while abroad so you don't lose a chunk of your cash to senseless commissions and fees. But really, you should [learn to avoid needless fees whenever you're traveling anywhere][travel_banking].
- You can't always stay in hostels and hotels and keep your sanity (or your budget!). We spent a large chunk of our time abroad staying at AirBnBs--enough that [I wrote a guide to using AirBnB if you're worried about getting started][travel_airbnb].
- We know tourism can easily ruin some of the most beautiful places in the world. We did our best to [avoid being Those Guys][travel_tourismgood] and generally be sensitive about our presence in other countries.
- How much 14 months of continuous travel actually cost us, given we weren't willing to travel like Super Hardcore #RealBackpackers. <span class="upcoming">Coming soon!</span>
- What coming back looks and feels like, and how we restarted our lives. <span class="upcoming">Coming soon!</span>

You can also check out <%= link_to "everything I've written about travel", category_path('travel') %>.

### Technological musings

It's been my job to keep up with technology for many years, but it became my passion long before that. Occasionally, that spurs me to think deeply about a subject and write it up.

- Password managers are great, but to really get them to hit the mainstream, [we need sites to adopt a common password management API][tech_pwdapi1]. Here's [a very high level overview of how I'd design it][tech_pwdapi2]. (I also [presented about this at Seattle's Ignite conference][tech_pwdvid]!)
- Speaking of APIs, there's an enormous difference between a company that releases a bolt-on API just because and [one that considers it their main product][tech_apiproduct], and treats it accordingly.
- When highly visible platforms like Twitter have policy issues, all sorts of people weigh in. Often, they don't understand [why policy discussions at an online platform are so hard][tech_twitterpolicies], nor what agents at those companies can do. <span class="upcoming">New!</span>
- After several years at Microsoft, I found myself caught up in the Microsoft hardware and software bubble. Here's [why I switched to Android and a Mac][tech_newmac].
- When a codebase grows organically over years or decades, you often end up with a giant monolith and then look to figure out how to refactor it into, say, microservices. There are two paths to approach that refactoring, [but only one of them is likely to work][tech_refactoring].
- When my wife and I planned our wedding, we were shocked at the complete failure of wedding planning tools to account for the fact that modern-day partners plan a wedding together. You can, instead, [plan your wedding the 21st century way][tech_wedding].
- Techies, myself included, often start to cast doubt on popular technologies just because they've gotten popular. It's [worth fighting that contrarian urge][tech_contrarian], though, because it hurts people who are just learning.

Here's <%= link_to "everything else I've written about technology", category_path('technology') %>.

### Jobs and careers

Your career is one of the most important aspects of your life, so I think it's important for everyone to share what they learn about the business world over time. Here are some of the top takeaways I've had over the past decade:

- No one really starts a job these days expecting they'll be working at the same company for even five years, let alone decades. But you might find yourself at a job much longer [if you find one that actually clicks][jobs_thatclick].
- Many teams of otherwise-competent people are plagued by communication problems. The solution is almost always to [broaden the groups you talk with][jobs_talking].
- We used to celebrate generalists, but the modern world is built to reward specialization. We should all [find our niche and demonstrate our expertise][jobs_specialize]. 
- All the way back in 2008 during my first real job hunt, as a sometime columnist for Georgia Tech's student newspaper, I was thinking about how you [find the right fit, not the "best job."][jobs_rightfit]

Want more of my thoughts about careers? <%= link_to "Here you go.", category_path('career') %> 

### The last thing I wrote

<% page_articles.each_with_index do |article, i| %>
<article>
	<h2 class="post_title on_page_post">
  		<%= article.title %> 
  		<span class="on_page_date"><%= article.date.strftime('%b %e, %Y') %></span>
  	</h2>
  	<h4 class="post_subtitle on_page_post">
  		<%= article.data.subtitle %>
  	</h4> 
	<!-- article content -->
    <div class="post_content on_page_post">
    	<%= article.summary %>
    </div>
    <div><%= link_to "Read more...", article %></div>
</article>
<% end %>

[travel_itinerary]: ./2016/08/going-where/ "Going where? Our round-the-world trip in a nutshell"
[travel_packinglist]: ./2016/08/packing-list/ "The short list: What does one bring on a long round-the-world trip?"
[travel_packinglist_followup]: ./2017/10/packing-list-winners-losers/ "What worked, what didn't: Not all the things I packed for our round the world trip survived the journey"
[travel_airbnb]: ./2017/04/airbnb-in-europe/ "Staying at AirBnBs: A brief guide to help you make the leap beyond hotels"
[travel_tourismgood]: ./2017/09/tourism-good-and-bad/ "Tourism, good and bad: Can tourists visit beautiful places without destroying what makes places worth visiting?"
[travel_banking]: ./2017/09/money-problems/ "More money, more problems: The complete guide to money while traveling"


[tech_pwdapi1]: ./2014/05/common-password-management-api-1/ "We need a common password management API"
[tech_pwdapi2]: ./2014/08/creating-a-common-password-api/ "Creating a common password management API"
[tech_pwdvid]: https://www.youtube.com/watch?v=fvJBRBggtyQ "Arcadiy at Ignite 24: Change ALL the passwords"
[tech_apiproduct]: ./2015/04/api-product/ "The API is the product: Companies have figured out how to make great APIs and make money"
[tech_newmac]: ./2014/11/the-cool-new-things/ "The cool new thing: How a Microsoft-using contrarian ended up with a Mac"
[tech_refactoring]: ./2015/06/soccer-ball-solar-system/ "The soccer ball and the solar system: Two ways to think about reworking a complex software architecture"
[tech_wedding]: ./2015/03/wedding-planning/ "Wedding planning the 21st century way"
[tech_contrarian]: ./2012/03/fighting-the-contrarian-urge/ "Fighting the contrarian urge"
[tech_twitterpolicies]: ./2017/11/twitter-policies/ "The conundrums of Twitter policy: You can't solve an online platform's support struggles in 280 characters"

[jobs_thatclick]: ./2016/07/coming-up-next/ "Coming up next: On jobs that click, and leaving them"
[jobs_talking]: ./2015/08/talking-at-work/ "Talking at work: Want a team to communicate well? Talk to groups at least 80% of the time"
[jobs_rightfit]: ./2008/09/strive-for-best-fit-not-best-job/ "Strive for best fit, not best job"
[jobs_specialize]: ./2017/10/find-your-career-niche/ "It's hard out there for a generalist: Want to get the best opportunities? Better commit to a specialty"

[about]: ./about "About Arcadiy"
[pic]: arcadiy.jpg "Arcadiy"