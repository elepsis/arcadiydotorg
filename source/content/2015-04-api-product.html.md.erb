---
title: The API is the product
subtitle: Companies have figured out how to make great APIs and make money
date: 2015-04-12
category: Technology
tags: api, programming
---

Back in the early days of Web 2.0 exuberance, all the hot new online services (Flickr, Delicious, and even Twitter) built largely open APIs and encouraged developers and customers to build [mashups][1]: fun but fleeting ways to juxtapose data from multiple sources on a single webpage. These companies seemed to believe that enabling people to create new and unexplored tools on top of their data would produce magical, unexpected innovation that would make the entire platform richer and ultimately accrue benefits to them. For a time, nearly every consumer startup seemed to launch with an API ready to go, even if their company didn't yet have enough usage to make developing on that API worth people's while.

Unfortunately, as the early mashup excitement waned, these companies figured out they had to earn money and exert more control over their platform. Watching Twitter stumble drunkenly through that process has been particularly entertaining, but the gradual falling into disrepair of many other companies' APIs may ultimately be worse for the web community as apps break and the features of the product diverge from the features of the API.

In recent years, though, a different approach to APIs has emerged. Companies like [Twilio][2] and [Stripe][3] are rehabilitating the idea of an API from the legacy of failed late-2000s startups and going "all in": the APIs these companies provide are not just important, but the core product that each of them sells.

These companies have figured out two fundamental things about APIs:

1. The best APIs aren't for your product but for a problem.
2. You must put as much effort into the user experience of developers who want to use your API as you do in all the other elements of your product.

READMORE

### APIs for the real world

Here's a very simple scenario: I am a developer, and I have a problem: I need users to confirm their identity. All the big companies, Google, Microsoft, etc., all seem to do it via a phone number; they ask for your phone number and send you a text message with a confirmation code. Well, how the heck do I send those confirmation texts to users? 

Enter Twilio. Setting the technology aside for a second, Twilio is **an API for interacting with people over phone or text**. If they are successful, the very first thought of any developer who needs to solve *any problem in that general category* will be, "hey, I bet Twilio does this." Indeed, we're already seeing this happen; Twilio is [reported to have hit $100 million in revenue run rate in 2014][4] and is expected to be headed for an IPO as increasing numbers of developers integrate with their APIs, all because their API solves a clear problem[^1]. 

And while they have a handful of competitors with similarly meaningless names--Sinch, Nexmo, Plivo--the market they've created should have belonged to the telecom industry itself. Unfortunately, the big telecom companies couldn't see the value of the API as a product, and now, when developers ask themselves how to best interact with people over phone or text, they don't have AT&T come to mind.

### Polish and more polish

Like Twilio, Stripe is an API for the real world: **an API for getting money from people**. That's an even more necessary piece of functionality for developers than phone or text, so when Stripe first arrived on the market there were already lots of companies happy and willing to work with developers. 

Unfortunately, most of those companies fundamentally saw their product as a set of financial services, and they spent their time trying to provide the broadest capabilities rather than focusing on what the developer experience would be. Stripe, instead, saw its API as its product, and put consumer software levels of polish into its entire experience. 

Getting started with Stripe doesn't require weeks of setup and verification like some of their competitors: a developer can integrate Stripe payments into their systems in a matter of hours. And richer integrations are about as pain-free as anything dealing with the financial industry ever is, thanks to Stripe's [well-documented APIs][5] and rich SDKs.

There's a reason that "Documentation" is one of the five top-level links at the top of every page on Stripe's site. That documentation, with examples in a number of languages side-by-side with the description of each component, is widely viewed as one of the best examples of API docs produced by any company.

![Stripe][picStripe]

Most importantly, it's clear that both Twilio and Stripe treat their APIs as a true product. Twilio explicitly [hires Product Managers][6] to build out a roadmap, talk to customers and thoughtfully expand its APIs, while many companies are content to leave their API story to be run directly by their engineers.[^2] Both companies also participate actively in the tech community, both by having their engineers attend conferences directly and by hiring developer evangelists to interact with their user base.

### The world's most obvious business model

The best thing about these API companies is that their focus on bridging the gap between software and real world functionality for developers enables a business model that is profitable and obvious: there's no need to invent a new way to monetize their large user base, a-la Google or Facebook, because the monetization strategy is to directly charge developers in exchange for doing things that provide clear and obvious value. 

Maciej Ceglowski, the founder of [Pinboard][8], made the case that [you don't want to be a free user][7], because those services are far more likely to go away or change dramatically as priorities change. That goes double for developers, who invest significant amounts of time and effort into any product they choose to integrate with. When the API is the product, and the company is getting paid each time you do something with that API, the probability of them making dramatic changes to said API drops dramatically.

In all of today's bubble speculation, I can't help but admire these companies for quietly accumulating users and revenue in an entirely "unsexy" way.

[^1]: Just because you have an API that solves a problem doesn't mean you can't have a larger product around it. Dropbox got traction for their API by solving the problem of "how do I easily sync my users' settings," but no one would say that was their primary product. Still, that was a great driver of usage for them.

[^2]: I should add that engineers can do all of those things competently if the culture at the company prioritizes them doing that work. Unfortunately that's rarely the case; more often, I've seen developers working without a PM change an API in the most expedient way to accomplish the task of the day, which inevitably leads to the API becoming an unmanageable, inconsistent mess.

[1]: http://www.wikiwand.com/en/Mashup_(web_application_hybrid) "Mashup on Wikipedia"
[2]: https://www.twilio.com/ "Twilio"
[3]: https://www.stripe.com/ "Stripe"
[4]: http://blogs.wsj.com/venturecapital/2015/02/20/twilio-positions-itself-for-an-ipo-after-logging-100m-in-2014-revenue/ "Twilio Positions Itself for an IPO After Logging $100M Revenue Run Rate"
[5]: https://stripe.com/docs/api "Stripe API documentation"
[6]: https://boards.greenhouse.io/twilio/jobs/25846 "Senior Product Manager at Twilio"
[7]: https://blog.pinboard.in/2011/12/don_t_be_a_free_user/ "Don't be a free user"
[8]: https://pinboard.in "Pinboard bookmarking service"

[picStripe]: <%= root_url %>/2015/04/api-product/stripe.png