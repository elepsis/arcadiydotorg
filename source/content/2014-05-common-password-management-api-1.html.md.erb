---
title: We need a common password management API
date: 2014-05-22
category: Technology
tags: api, password, pm
---

*Note:* On May 22 I spoke on this topic at [Ignite Seattle 24][1]. If you're familiar with the Ignite format, you'll realize that it demands conciseness, so I wanted to expand on my talk.

This is part one of the expanded write-up.

### Background

A few weeks ago, a pretty nasty vulnerability in the OpenSSL software that's used by many popular websites on the internet was discovered. It quickly became known as the [Heartbleed bug][2], and a security company called Codenomicon compiled a fantastic write-up on its impact and how to recover. To quote from their site:

> The Heartbleed Bug is a serious vulnerability in the popular OpenSSL cryptographic software library. This weakness allows stealing the information protected, under normal conditions, by the SSL/TLS encryption used to secure the Internet. SSL/TLS provides communication security and privacy over the Internet for applications such as web, email, instant messaging (IM) and some virtual private networks (VPNs).
> The Heartbleed bug allows anyone on the Internet to read the memory of the systems protected by the vulnerable versions of the OpenSSL software. This compromises the secret keys used to identify the service providers and to encrypt the traffic, the names and passwords of the users and the actual content. This allows attackers to eavesdrop on communications, steal data directly from the services and users and to impersonate services and users.

As you can likely tell, this bug was bad news for thousands of system administrators who had to scramble to figure out if they were affected or could have been affected. But it actually gets worse, because as a result of the nature of the bug, a vulnerable site **cannot determine** whether they have been exploited or not: an attacker can come in, exploit Heartbleed, and get away with some of a site's crucial data, all without leaving a trace.

As a result, the **only safe assumption** any system administrator could make is: if my site was vulnerable, I have to assume it's been exploited. Cue massive sysadmin panic.

### The user impact of Heartbleed

Because the OpenSSL library is so common across the internet, many many sites were affected by this bug. And because system administrators could not be sure if their sites were actually exploited (but had to assume they had been), users also had to assume that their data on any vulnerable site&#151;particularly their passwords&#151;were compromised. As a consequence, widespread media reports popped up advising users to **change all their passwords now**!

In practice, this wasn't fantastically good advice, because users actually needed to wait for sites to update in order to ensure they were once again secure, lest they change their password only to have it stolen the following day. But much of the press coverage nevertheless advised a blanket password change.

### Changing all your passwords is pain and misery

The problem with this advice is, of course, that for just about all users, changing their passwords is an exercise in frustration. Oftentimes it goes something like this:

1. Go to site 1/32 that you need to change passwords at.
2. Try to log in with a password you don't fully remember.
3. Go through the multiple-step "forgot password" flow to recover your old password.
4. Finally sign in.
5. Change your password.

And of course, a few days later you go back to the site where you hit the awkward final step of the password change process:

<ol start="6">
	<li>Try to sign in and wonder "What the heck did I change my password to?!"</li>
</ol>

So yeah, no fun.

### What can you do today to make this better?

You can already make your life better by going one of several routes to change how you deal with passwords. The obvious choices are to use a password manager app, or to sign in everywhere you can with a common account like your Facebook or Twitter.

#### Using a password manager

There are several different "password manager" apps that exist, including [LastPass][3] and [1Password][4]. These apps integrate with your browser to automate a few password related tasks for you:

1. When you sign up for a new website, they will automatically generate a unique (and secure) password for that site.
2. When you subsequently go to sign in to the site, they'll automatically populate your secure password into the password field so you can easily sign in.

These features alone are sufficiently compelling that many security experts recommend using a password manager *in general*. However, password managers do have several drawbacks. Using them on mobile tends to cost money or is a pain, and they can cause "but what if I need to log in at a friend's house/at an internet cafe" stress. But most critically, they're password *managers* in name only: when you need to change your password, you still have to **manually** go to every site and update your password. For many users, myself included, this just makes them not worth the trouble.

#### Sign in with Facebook, Twitter, or similar

The least painful, most "dad-friendly" way to minimize the number of passwords you have to worry about is to just sign in everywhere you can with your Facebook, Twitter or (one of a half dozen other services) account.

Unfortunately, this has a few major drawbacks.

First and foremost, you're putting all of your eggs into one basket: hopefully one that's relatively well-secured, but one basket nevertheless. This makes your Facebook account a juicy target, but more importantly it leaves you vulnerable to the whims of a single company deciding to, say, close or suspend your account for whatever reason.

It can also lead to a different kind of awkwardness when you visit a site: the question of "oh man, did I sign up here with Twitter or Facebook or what?!" I can say from my own experience that I have this dilemma depressingly often.

Finally, it's simply not a solution for all of your password needs. Your bank is probably never going to let you sign in with your Facebook account, and this is probably for the best.

### So if those two options aren't good enough, what can we do?

As an end user, not much&#151;at least not yet. However, if you're in a position of power at one of the major players on the web, there's one thing you could do that will make this easier for everyone in the long term: **work to standardize how different sites across the internet allow a user to interact with their passwords**.

There are likely many approaches one could take to standardization, but the one I'd like to propose is a common password management API. Stay tuned for more details on how, why, and why I'm not crazy. :)

[1]: http://igniteseattle.com/2014/05/13/speaker-lineup-for-ignite-24-tickets-on-sale/ "Ignite Seattle 24"
[2]: http://heartbleed.com/ "Heartbleed"
[3]: https://lastpass.com/ "LastPass"
[4]: https://agilebits.com/onepassword "1Password"