---
title: What worked, what didn't
subtitle: Not all the things I packed for our round the world trip survived the journey
date: 2017-10-11
category: Travel
tags: packing
---

Just after our departure for the start of our trip in August 2016, I wrote a [long blog post cataloguing every single item I packed for this trip][plist_post]. Now, over a year later, I can pretty confidently reflect on that initial list of stuff and share what I learned. Some things worked even better than expected; others I had to replace not long into the trip. On the whole, though, we've been pleasantly surprised with how well the stuff we brought has held up.

I'm not going to talk about every single thing I brought in this post; you can read the original for that. Rather, I'll focus on the top items that stand out among the things I kept and the ones I didn't.

READMORE

Note: most of these links are referral links. Gotta earn back the cost of the trip somehow!

### Winners

I did a lot of research before we left, so most of the products I ended up getting for this trip worked out pretty well. Still, some things worked out so well that I wouldn't travel again without them. Here are ten standouts: 

#### [Zojirushi travel mug][zoji]

![Zojirushi travel mug][picZoji]

The Sweethome lists a similar Zojirushi model as [their best travel mug][sweethome_zoji], so you know it's a quality product, but does it make sense to lug an insulated mug around the world? Absolutely. I used my Zoji wondercylinder every single day, and not just because I drink a lot of tea. In colder climates, I'd head out each morning with 16 ounces of delightfully hot tea, but once our travels reached the hotter weather in Asia I'd often fill it with cold water, which it would keep icy for hours. The Zojirushi also came in handy in several hotel rooms that didn't have reasonable mugs, and I also often steep tea in my thermos and then pour it in cups for drinking. 
	
#### [Sunday Afternoons Sun Tripper hat][sunhat]

![Suntripper hat][picSuntripper]

I love this hat even for daily life and own several, but it's especially good for travel. It's designed to keep your head cool in the sun, so it has a few fantastic features: it's rather well ventilated so your head doesn't sweat as much and you overheat less; the underside of the bill is a darker color to make it easier to see in bright places, and it's rated for UPF 50 sun protection. Plus, the bill folds in half so the entire hat makes a tiny package in your bag.
	
#### [Eagle Creek compressible packing cubes][cubes]

![My raincoat inside an Eagle Creek packing cube][picCubes]

If you're frequently jumping from place to place, you *need* packing cubes. With the cubes in your bag, packing and unpacking becomes predictable: stuff never shifts around weirdly making your bag unexpectedly uncloseable; you know exactly where your undies are, and you never forget anything in the dresser drawer.
	
These are easily the best packing cubes I've ever encountered, and both my wife and I have used them for all of our clothing throughout our travels. They're ultra-light, they hold a ton of stuff (especially if you ever choose to carry your stuff uncompressed, though we don't), and they've held up brilliantly over a year of being stuffed into our bags. The medium size, thanks to the compression feature, is also great for storing rain coats and puffy jackets in the most compact possible form.
	
#### [Uniqlo Heat Tech shirts][heattech]

![Uniqlo HeatTech t-shirt][picHeatTech]

These shirts were a relatively late discovery, but they're now my favorite travel t-shirt. The fabric is thin so they dry quickly and pack very compactly, but not so thin that they're transparent in the sun, they come in a handful of basic colors that go with everything, and you can occasionally find them on sale for $7-8 a shirt. What's not to love?
	
#### [Ibex Shak Hoodoo hoodie][ibex]

![High quality wool on this jacket][picIbex]

This is a 100% wool hoodie, so it is one of the most expensive items in my wardrobe. But it's also one that brings a rare combination of light weight, compactness, warmth and comfort in a single package. When we had to survive -30 degree weather in Moscow in January, this plus my Arcteryx Atom LT puffy jacket (which I also wholly recommend) kept me from freezing to death, and in more moderate weather it served as the perfect out-and-about companion.
	
#### [Ecco Biom Grip II shoes][eccos]

![A fairly well-worn shoe][picEccos]

When I think about how long shoes are supposed to last, I generally think in terms of time: months or years, depending on the shoes. But in reality that time is just an approximation for use, and I've had to keep reminding myself of this as I've put as much mileage on my shoes in the past 15 months as I normally would in two or three years. Luckily, my Ecco Bioms held up shockingly well. They're definitely looking well-loved now, and I won't be wearing them past the trip, but any shoe that survives countless 20,000 step days in vastly different climates, works great for both light hiking and city walks, and is comfortable enough out of the box to wear without break-in deserves massive praise. Unfortunately Ecco seems to have discontinued this model in the U.S. (as they do seemingly every year, breaking my heart).

#### [Linen towel][linentowel]

![My linen towel in its packing cube][picTowel]

I can probably count the number of times I've had to use my towel during the past year on two hands, but I have absolutely no regrets about having brought it. After all, there's no good alternative to having one when you *do* need it, so you can carry one or search out new ones every time the need comes up (which, coincidentally, is almost always at times you wouldn't expect). Besides, you know what they say about traveling with a towel.
	
Maybe you can guarantee that you won't need one, but if you do have to have a towel, you might as well have a nice, compact one that doesn't suffer from the biggest flaw with most travel towels: The Stink. Modern travel towels pack down to tiny sizes and dry super quickly, but god help you if you don't let it get 100% dry before having to pack it away: that towel will smell like death itself by the next time you take it out of your bag. And given I like to shower in the mornings, that often means I end up with a partially moist towel for the day. So get a linen towel and get 90% of the portability with 0% of the smelliness.
	
#### [Muji travel containers][mujitravel]

![Shampoo and shaving cream in just two of my Muji travel containers][picMuji]

Having done a comprehensive round-the-world trip with these and also having used a variety of other containers out of necessity, I feel pretty confident in saying that these Muji products are the best travel liquid containers you can buy. 
	
Muji is the only company I've encountered that's figured out that you want your travel containers to be compact, fit properly into a ziplock bag, and not poke holes in said bag. Almost every other travel container I've seen violates one of these, either with giant-sized lids that make them needlessly hard to pack or sharp, pointy corners. Muji, on the other hand, gives you a beautiful tapered cylinder. It works great, and a bunch of these fit into a baggie perfectly.

Plus, of course, it doesn't leak (even when stuffed into a full bag), holds up to lots of opening and closing of the snap-on lid, and conveniently stands up in the shower. These are so good that I'm buying a whole bunch of spares while we're in Japan and they're available super cheaply. Get them!

#### [Baggu shopping bags][baggus]

![Baggu reusable shopping bag][picBaggu]

I started the trip with a cheap freebie reusable bag I'd gotten at a conference, and it ripped about a month into the trip. So I replaced it with something a little higher quality: these delightfully compact yet strong bags from Baggu. My wife and I each have one, and we've used them to carry vast amounts of groceries, beers or various miscellany in moments where we've temporarily overflowed our suitcases. They're a little on the pricy side, but they both still look brand new after a year of frequent abuse. And they are small enough to fit into any bag you might be carrying, or even a pocket in a pinch, so we'll continue to give them a workout once we're back in Seattle.
	
#### [Sea to Summit travel clothesline][travelclothesline]

![Sea to Summit travel clothesline][picClothesline]

I found this tiny clothesline at REI just before our departure, and it's worked out really nicely for us. It's tiny enough that you barely notice it in your bag, but it stretches out pretty widely and is easy to attach by wrapping around balcony railings, shelving or any other object you can come by. The little grabby beads work well at holding clothes up, and in the few places we've been where we didn't have sufficient hangers or a clothesline at all it's been a godsend.

### Losers

In general, the losers in my packing kit have been things where I cheaped out and didn't get the thing I should have gotten in the first place. Live and learn, self. Live and learn. But all in all, you'll notice this list is thankfully much shorter than the list of winners.

#### Mid-range cell phone (Sony Xperia Z3 Compact)

I'd had this phone for just under a year before we left on our trip, and it was fine for my daily life around Seattle. I thought it would suffice through our travels, too. I was completely wrong. 
	
With T-Mobile's free global data and us being out and about constantly, I found myself more dependent on my phone than ever before, and every time my Xperia slowed down because I was running up against limits in its storage capacity or just because it was old and full of cruft, we would get stuck, lost on a street corner. It turns out that when your phone is one of the most vital computing devices in your life, it just makes sense to shell out the money for a flagship.
	
I ended up replacing my Xperia with a [Samsung Galaxy S7][galaxys7] while we were back in the U.S. last September, and that phone has held up fantastically and worked way better than I expected. I'd been skeptical of Samsung phones previously based on the Galaxy line's early days, but now I'm fully aboard the Galaxy express.

#### Main backpack (Northface Overhaul 40)

When you spend a year living out of your bag, that bag is enormously important to your quality of life. So I'm sad to say that my main bag just didn't work out for me. Don't get me wrong, there was nothing that went disastrously wrong with it. In many respects it was actually a perfectly fine bag, and it would have held up through the end of the trip if I weren't tired of dealing with its design flaws. But those design flaws turned out to be such an ongoing irritation that when the opportunity came up to switch to something else, I had to jump on it.

What was so wrong with it? It was just way too easy to pack in such a way that nothing fit and vast swaths of space were left empty, and painfully hard to actually fill it up properly. I regularly had to rearrange my things and force them down into the main compartment, and no matter what I did, my wife's bag (which was ostensibly the same size) consistently fit way more stuff way more comfortably. The solution to this is actually quite simple, and this is what I learned after months of struggles: do not buy a travel backpack that has any design other than panel loading (where the entire front or back of the bag opens). All other methods of packing are going to be too much of a pain.

I replaced the bag with a [Gregory Compass 40 backpack][gregorypack], which is equally well constructed and nicely designed. This one does in fact open properly and therefore fits all my things without a long game of Tetris, and I was able to get it for dirt cheap because apparently no one wanted the mustard yellow color.
	
#### Zolt charger

Boy am I sad this one did not work out. I am not a fan of the design of Apple's chargers, and especially for travel their un-detachable cords end up ruining their otherwise-convenient box shape. So the Zolt should have been perfect: it was compact, its cable was detachable, and it could charge two USB devices at the same time as the laptop.
	
The reality is it was anything but. There were a bunch of things wrong with it from day one: it often sparked when being plugged in, any time it was plugged in and not actively charging it would make a loud buzz, and it occasionally overheated and shut down without successfully finishing charging my laptop. And, to be clear, I'm not talking about a laptop that draws massive amounts of power: my MacBook Air uses Apple's smallest, 45W MagSafe charger. For all of these reasons I was never even comfortable leaving it plugged in while we left our room.
	
Ultimately I discovered that unlike almost every other piece of electric equipment in the 21st century, which is rated for 100-240V to work worldwide, the Zolt was only rated for 120-240V. It worked flakily in a few countries that run on 110V, and finally died outright in Japan, which goes as low as 110V. (I then had to go buy a replacement Apple charger for stupid amounts of money.)

It turned out that the company behind the Zolt (predictably) went out of business and you can now buy them for like $35. I still wouldn't pay that money for this mediocre and potentially dangerous product. Thankfully, my next laptop will charge with USB-C, so I'll be able to replace my charger more affordably in the future, but the lesson I've learned is to stick to chargers from major laptop manufacturers *only*.

#### REI ultra-light merino socks
	
When I bought a bunch of these ultra-light socks I was thinking they'd be perfect for the hotter places we were visiting, as they were thin enough that they wouldn't overheat my feet. Somehow, that's not how it worked out: Merino turns out to be breathable enough that thicker socks didn't really bother me from a temperature perspective, but the lack of padding and general thinness of these socks caused my feet to rub against things weirdly and get blisters. They also started wearing through faster than I'd like.
	
I think the problem has more to do with ultra-light versus light socks than with the brand, but I replaced these with [SmartWool light outdoor socks][smartwool], and they've held up really nicely throughout the trip. While they weren't quite warm enough for standing around outside in the middle of winter, I'm not sure any sock that could fit in my shoes would have been: the temperature being well below what my shoes were meant to handle was the real problem there.

#### Anything I already had before the trip
	
This is kind of a catch-all category, but there was a noticeable pattern with some of my belongings: just about anything I'd bought and had for a while before leaving for this trip didn't hold up for the whole trip. The abuses on a small set of stuff in over a year of continuous travel are severe, and if it starts out well-loved it's unlikely to hold up throughout the whole journey. I had to replace several things at inopportune moments because I wore holes through them or had them give out outright, and while it wasn't really a problem, it would have been nicer if I'd just left with all new stuff. In particular, I had to replace:

- My flip flops, which had been subjected to relatively mild usage over several years. After looking around for a long time for something equally comfortable and hopefully cheaper, I resigned myself to buying [another pair of OluKai flip flops][olukais], albeit their entry level model. Their flip flops remain the most comfortable I've ever worn.
- Most of my underwear and one pair of shorts. In general, I underestimated how often I'd wear shorts versus pants on this trip. Much of Asia is hot year round, y'all.
- Spork: My plastic spork cracked in my bag, having been pressured by too much crap in it one time too many. I replaced it with one made out of titanium so that doesn't happen again.
		
### Life lessons

Other than specific items that did or didn't work out, there are a few general lessons I learned about packing for a trip like this.
 
#### You don't need twelve backups

One of the biggest mistakes I made early on in the trip is being a bit *too* paranoid. I didn't want to get caught in a situation where I needed to replace something inconveniently, so I brought spares for everything: cables, headphones, pens, glasses, hat, chargers, etc. I'd get rid of most of them. The glasses are the only item on that list that aren't easy to find an adequate replacement for on demand, and while I still believe it's useful to have a spare for most of those, the key is *one* spare. I at one point had something like five micro USB cables, and no one needs that.

#### Ease of laundry is key to happiness

To be clear, I knew from the get go that we'd be doing a fair bit of laundry during our trip: you can only pack so many shirts. What I expected, though, is that we would do a lot of it by hand, and indeed that's what we did during most of our first month. Unfortunately, it turns out that we *hate* hand-washing our clothes. Those of you who have done this kind of travel and done your laundry every other day in a hundred different sinks that may or may not have adequate stopper capability: my hat is off to you, but my life is too short for that nonsense.

After a month of pain we realized we just needed to prioritize booking hotels and AirBnBs which had a washer available for our own sanity, and it turned out to be not actually all that difficult in most countries. We also started regularly carrying a bag of laundry pods, which are not ideal if you're traveling only with carry-ons but a lot more space efficient than powdered detergents. Did we check our luggage more often than we would have otherwise because of the laundry pods? Yes. Was it worth it? Double yes. And in the meantime, the hand-washing detergent we were carrying went to waste.

#### Bring the extra set of daily wear

If you are one of the aforementioned people who can commit to doing their laundry every other day, this advice does not apply, but if you want to have a little less of your life revolve around laundry, just go ahead and bring a few more dailywear items than you might need otherwise. I had originally intended to travel with just five shirts and underwears, and now I'm up to seven of each. The extra weight and space is negligible, but the extra clothes let me have entire cities where we don't do laundry (given we usually tried for a minimum of 3-4 nights in a place), opening up more booking options and generally preserving my sanity.

#### Standardize your liquids

I already said in my original post that standardization helps, but I want to reiterate this especially in the context of travel containers. I'm working on going all in on the Muji travel containers I called out above because they're fantastic, but also because your toiletry bag looks and works better when all (or almost all) your containers are the same shape and size. Again, your life will get much easier if you can minimize how often you have to play Tetris, and oddly shaped travel containers are another frequent initiator of that game.

#### Spice up your life (and cooking)

We found ourselves in AirBnBs or common kitchens with lacking spices often enough that we were constantly buying (or going without) the same things. I ultimately ended up building up a mini travel set of spices to supplement our usual needs: a travel-sized bottle of salt, some tiny bottles of olive oil we found in Spain, and a little Muji travel container (in the smaller size) of Sriracha. With just those three items, we can cook ourselves flavorful meals without needing to buy something from the store every time.

#### Don't hold on too long

There are lots of items I waited longer than I should have to replace. When you live a hectic travel lifestyle and you only have so many things, you have to assume that some of them will die as you go. If you still have your clothes by the time they start getting actual full-on holes in them, you've probably kept them longer than you should have, and you should be more proactive in throwing them away and replacing them as you can.

Similarly, I will get rid of almost all of my daily clothing I wore on this trip upon my return (with the exception of the things I replaced recently, of course). While it's held up well enough that I'm not ashamed to be seen wearing it, almost all of the daily wear items are showing signs of their hard life, and most of it is probably going to start failing outright in the not too distant future. Sometimes it's best to just let go.

### In the end it's personal

There are lots of other things I love but that I don't recommend here because they're fairly esoteric. My daybag I acquired the last time I was in Japan (and which I just replaced with an updated model after my original absorbed so much of my sweat it had gained a pound), my Travelers Notebook and my shoehorn are just three of the things that I wouldn't travel without, but that reflect my specific needs and desires and are therefore not something I recommend broadly. 

I'm sure any other traveler you speak with would have their own list of things that are meaningful to them, too. I know lots of people travel with coffee-making equipment, which sounds insane to a non-coffee drinker like me. If good coffee is going to make you happy every day, though, of course it's worth bringing the things you need to make that happen. So take all of these recommendations with a grain of salt. I can tell you what made sense for me, but only you can figure out what makes sense for you.


[plist_post]: ../../../2016/08/packing-list/ "The short list: What does one bring on a long round-the-world trip?"
[sweethome_zoji]: http://thesweethome.com/reviews/best-travel-mug/ "The Best Travel Mug - The Sweethome"
[zoji]: http://amzn.to/2zfSWpO "Zojirushi Stainless Steel Travel Mug"
[sunhat]: http://amzn.to/2gcZssX "Sunday Afternoons Sun Tripper Hat"
[cubes]: http://amzn.to/2xzva6a "Eagle Creek Pack-It Specter Compression Cube Set - 2pc Set"
[heattech]: https://www.uniqlo.com/us/en/men-heattech-crewneck-t-shirt-short-sleeve-172753.html "Men's HeatTech Crew Neck T-shirt (Short Sleeve) - Uniqlo"
[ibex]: https://www.ibex.com/mens-shak-hoodoo-hoody "Ibex Shak Hoodoo Hoody"
[eccos]: http://amzn.to/2xza1sV "ECCO Men's Biom Grip II Fashion Sneaker"
[linentowel]: https://www.etsy.com/shop/LinenStory?ref=l2-shopheader-name "LinenStory on Etsy"
[mujitravel]: http://www.muji.us/store/pe-tube-50g.html "Muji - PE Tube 50g"
[baggus]: http://amzn.to/2xyRtJk "BAGGU Standard Reusable Shopping Bag"
[travelclothesline]: http://amzn.to/2xzci7r "Sea to Summit Lite Line Clothesline"

[galaxys7]: http://amzn.to/2kGoMZB "Samsung Galaxy S7 Smartphone"
[gregorypack]: http://amzn.to/2yZIhP2 "Gregory Compass 40 Daypack"
[smartwool]: http://amzn.to/2yZqyaB "Smartwool Men's PhD Outdoor Light Mini Socks"
[olukais]: http://amzn.to/2yZkkaD "OluKai Ohana Sandal - Men's"

[picEccos]: <%= root_url %>/2017/10/packing-list-winners-losers/ecco_shoe.jpg
[picCubes]: <%= root_url %>/2017/10/packing-list-winners-losers/packing_cube.jpg
[picMuji]: <%= root_url %>/2017/10/packing-list-winners-losers/muji_containers.jpg
[picIbex]: <%= root_url %>/2017/10/packing-list-winners-losers/ibex_wool.jpg
[picHeatTech]: <%= root_url %>/2017/10/packing-list-winners-losers/uniqlo_heattech.jpg
[picSuntripper]: <%= root_url %>/2017/10/packing-list-winners-losers/suntripper_hat.jpg
[picZoji]: <%= root_url %>/2017/10/packing-list-winners-losers/zojirushi_mug.jpg
[picTowel]: <%= root_url %>/2017/10/packing-list-winners-losers/linen_towel.jpg
[picBaggu]: <%= root_url %>/2017/10/packing-list-winners-losers/baggu.jpg
[picClothesline]: <%= root_url %>/2017/10/packing-list-winners-losers/clothesline.jpg