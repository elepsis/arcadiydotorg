---
title: Coming up next
subtitle: On jobs that click, and leaving them
date: 2016-07-16
category: Career
tags: career, travel
---

When I started as a PM at Microsoft in mid-May seven years ago, I thought to myself, "well, I guess I'll give this a shot for a year, and if I don't like it, I have a 'Plan B' to go work abroad through [AIESEC][1]." And if I did like it, I figured I'd put in a couple of years before figuring out my next steps.

Well, it turned out that I did like it, and a "couple of years" rapidly became the better part of a decade. Why? Because I was lucky enough to find a job that clicked for me. And yet, after all this time, and even though both my family and some of my friends think I'm a bit crazy, yesterday was my last day at Microsoft.

READMORE

### What makes a job click?

Unlike most recipes, you only need to find three ingredients. Unfortunately, and also unlike most recipes, you can't just buy them at the store. If you do find all three, though, hold on: you're in for some of the most productive times of your career.

![Venn diagram of what makes a job click][picTheClick]

#### The right role

While different people may prioritize these in a different order than I do, the single most important thing in my mind is finding a role that actually works for you.

While there are exceptions, most people don't graduate from college knowing exactly what they're capable of, and the skills they have aren't necessarily aligned with the ones that are needed to do any real-life job well. So if you find a role where the set of miscellaneous skills you've acquired over the years covers a decent percentage of the skills you need, *and* one where the skills you don't yet have are ones you want to acquire, that might be the right role for you.

I went through most of college not knowing PMs even existed, let alone understanding what they did. And yet, when I learned about it at a Microsoft presentation, I pretty quickly realized that it was the job I found most interesting in the software world. So when I got to do it for real, I suppose I shouldn't have been shocked when it felt like putting on a favorite sweater.

#### The right team

It's a huge cliche to talk about the people you work with being what makes a job great, but cliches do exist for a reason. For me, there are two key things that go into making a great team: one that's based on the company culture as a whole, and the other based on the specific people you work with day-in and day-out.

The best thing you can hope for at the company level in a massive corporation, in my experience, is to be able to enter every interaction with your colleagues with the well-deserved assumption of competence. The best companies hire from a diverse pool of people who have just one thing in common: they know what they're doing, and if they don't, they pretty quickly filter out. People who aren't engaged in whatever they're working on can pose a challenge, but they're not nearly as damaging to the company as a whole as people who are just fundamentally in over their head: if you're not into what you're doing, at least you won't use up everybody else's time on remedial education.

At a more narrow level, there exist mountains of literature on [what makes a team work in general][2], and the spoiler alert is that it's rarely the same thing for all teams. The thing that makes team click for me personally, though, is openness, and the cohesion that arises from that openness. On each of my teams I've pushed hard for transparency and overcommunication, because I think everyone knowing what everyone else is doing (or at least being able to easily find out) is the first step in being able to fill in for one another, to be able to give constructive feedback and to build the whole-product understanding that is so critical for a PM to make intelligent decisions.

#### The right product

While I've known people who are perfectly happy to work on anything as long as they like the tasks they have to accomplish and the people they do them with, that isn't enough: I think people also have to *Believe*.

First, people have to believe in the problems they're trying to solve: if you think the things you work on are trivial or, worse yet, actively damaging to the world you want to see, how can you be expected to go the extra mile? That's not to say that you need to jump straight to the Silicon Valley-itis of thinking every single startup is Changing The World. You just have to find the problems your team and company are solving interesting and meaningful.

Second, they have to believe in the feasibility of a team actually achieving its stated goals. There's nothing worse than a deathmarch to build a bridge to nowhere, sold as a wonderful connection between two locales. If the team makes progress in solving its goals, is competitive in its market, and demonstrates a pattern of encouraging decisions, they can foster that belief in the tractability of the challenge they've taken on.

Personally, once I'm able to buy into the product at that level, surrounded by great people, and doing the things I'm good at, I find myself doing my best work: **that's the click**. That's the thing to look for in every opportunity.

### So... why would you ever leave?

While I couldn't capture all three of these ingredients at every moment of my time at Microsoft, I was fortunate enough to have this combination arise more often than not. As a result, I was able to do a lot of work I'm quite proud of, and enjoy myself along the way.

I must also note: say what you will about Microsoft's culture and competence in terms of building software, but Microsoft as an employer has its shit together to an absolutely astonishing degree. This applies to everything from benefits to just working like a well-oiled machine in its internal processes. The unsung HR heroes and admins who make this possible don't get nearly enough credit.

And yet in spite of all that, I'm leaving, and my answer to "why leave" is simple: I'm doing it because I'm incredibly fortunate to be able to fulfill a life-long dream. **My wife and I will spend the next year traveling through Europe and Asia, with only our (carry-on sized) backpacks in tow.**

![Backpackers painted while backpacking][picTravelers]

### Want to keep tabs on me?

We leave Seattle around the end of the month, spend a few weeks with our families, and then start our adventure in Dublin in the middle of August.

While we are incredibly excited to take this leap, many people have asked us how we can be so daring. The reality, though, is that we are well aware that [we're not brave, just lucky][3]. Although I do believe that many more people can travel for extended durations if they make it a top priority, the sacrifices involved for most people are far greater than those we've had to make given our software engineering incomes and lack of kids.

In light of my impending travel, you can expect two things to change here:

* I'll be writing a lot more often in general, since I'll be "funemployed," and
* I'll be writing more about travel (as I once used to) and less about jobs or technology, though I'd really like to strike something like a 50/50 balance.

My wife will also be posting updates on our trip, though where exactly is still a work in progress. However, if you want to keep up with **both** of us in one easy way, we'll be sending a weekly email update on our adventures. [You can sign up][4]--it'll only take a second!

[1]: http://www.aiesec.org "AIESEC"
[2]: http://www.nytimes.com/2016/02/28/magazine/what-google-learned-from-its-quest-to-build-the-perfect-team.html "What Google learned from its quest to build the perfect team"
[3]: http://www.everywhereist.com/quitting-your-job-to-travel-isnt-brave-its-lucky/ "Quitting your job to travel isn't brave, it's lucky"
[4]: http://tinyletter.com/arcadiyandgina "Gina and Arcadiy's Global Update"

[picTheClick]: <%= root_url %>/2016/07/coming-up-next/the_click.png
[picTravelers]: <%= root_url %>/2016/07/coming-up-next/italy-venice-travelers.jpg
