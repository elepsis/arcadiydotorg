---
title: End of a world view
subtitle: The things the world no longer values mean we're in for a long ride
date: 2016-11-10
category: Other
tags: politics, society, values
---

So you might have heard Donald Trump was elected to be the next president of the United States of America earlier this week, narrowly losing the popular vote but dominating the vote in rural areas to such an extent that his electoral victory was assured.

It was a shock to most people who watched&mdash;even to most of the analytics folks within the Republican National Committee and the Trump Campaign itself, who were, to everybody's knowledge, quietly preparing for defeat. The only people who were confident of Trump's victory were the people who had utterly embraced the information bubble surrounding Breitbart.com and its ilk.

And it was a body blow to a whole bunch of different groups who believed fundamentally that America was better than this: that the electorate would not reward a candidate who had attacked women, immigrants, people of color, the disabled and anyone who dared criticize him while voting down a vastly more deserving, more qualified woman, in no small part because of her gender.

As a white guy who is essentially the least impacted by Trump's racism, misogyny and casual hatred, though, I can't say that I felt that blow as viscerally as those of my friends and loved ones who are in one of the groups he's targeted[^1]. For everyone who takes this election as a rejection of these fundamental parts of their identities and now feels afraid for their basic well-being, I am truly sorry. I don't mean to gloss over it; I just don't feel it is *my* place to make these points.

Here's what I can say for myself, though: **this election rejected some of my most fundamental beliefs about what it takes to be a leader**, and I've spent the past day just trying to process what it means. Beliefs, of course, are a hell of a lot easier to change than where you're born or what gender you are. But because they are selected, not inherent, they are also in many ways core to your identity: they're a reflection of the way you think about the world.

To be clear, when you talk to Trump supporters, they won't say they voted for him as a conscious rejection of these beliefs, and they're being entirely honest about that. But the results nevertheless show a weighing of priorities: to Trump voters these things mattered less than certain other things. And that makes it utterly clear that my priorities don't match theirs.

I didn't reject Trump because of his sexism, racism or any of the other -isms he was consistently guilty of (though I also found those things abhorrent). I rejected him before even considering those, because he rejected all of these:

READMORE

#### Preparation and planning

There's no denying that Hillary Clinton was vastly more prepared to govern the country from day one than Trump is. She had detailed policy proposals for essentially every element of her agenda, and it was clear what the objectives of her presidency would be. With Trump, we have something that can at best be described as a single-page rough draft, with much of the outline scratched out or contradictory.

Absolutely no one (possibly including Trump himself) fully grasps what he actually intends to do or how he intends to govern, which may mean that he will lean on the Republican-controlled Congress for his legislative agenda. Okay, I don't love much of that platform, but fine. What about the aspects of the executive branch that aren't controlled by the legislature? Much like when Britain voted for Brexit with no plan in mind, Americans voted [for a candidate without one to govern][1].

Trump also made a complete mockery of the need to prepare for major speeches or debates, as demonstrated amply in his debate performances and in his consistent off-script asides where he repeatedly failed to thoughtfully consider his words and what they implied. He ran his campaign by the seat of his pants, and incredibly, that was enough.

**The takeaway is that America doesn't value preparation.**

#### Truth-seeking and transparency

Donald Trump has never met a number he wouldn't exaggerate, and that's just the tip of the iceberg on the flagrant lies he told. Highest murder rate in decades? Complete bullshit. Our inner cities are hell? Plays really well to the rural base! Hillary would let in 650 million people in a week? Sure, why not? The sheer quantity of lies Trump repeated at every step of the way was so vast that no one could keep up with them. And he paid absolutely no price for his assault on the truth.

The truth is, the confluence of the internet and politics is a scary place. As our filter bubbles grow, some people's are dominated by facts and quality reporting and others' are full of unsourced magical thinking. Sometimes it's outright fake (and probably [promoted by Macedonians][2]&mdash;go figure), but other times it's just a particularly twisted interpretation of real information.

The worst part is that depending on who you are and what you see, you might be putting completely opposite publications into the buckets I've outlined. This is the unsurprising result of the long-term project to discredit absolutely all of our media institutions, ensuring there's no one source the entire country can turn to as a publication you can trust to make an honest attempt at determining the truth. With Trump, though, even the conservative press bifurcated into the people who tried to keep at least some semblance of fact checking in place and those who wholly embraced Trump's message, no matter how truthful it might have been.

That's the tip of the iceberg with Trump, though. The larger question is how we could get any semblance of an understanding of his financial and business interests, something he's refused to publicize, and the conflicts of interest he'll have given the fact he's refused to put his corporate assets into a blind trust like, e.g. Michael Bloomberg did when he was mayor of New York City.
How can we as a nation determine whether a foreign policy decision he's making is based on something that makes sense for the country or something that makes sense for his quid-pro-quo business projects? I'm not suggesting ominously that he's going to automatically prioritize himself&mdash;just that we have no way to determine whether he is.

**The takeaway is that America doesn't value honesty.**

#### Intelligence and respect for the knowledge of others

When you become the official candidate for President for one of America's major political parties, one of the "perks" is that you get intelligence briefings from the vast intelligence apparatus in the U.S. It's not nearly as much information as the president gets, but the intent is to start building an understanding of the state of affairs among candidates.

Donald Trump has repeatedly dismissed the information provided to him in these intelligence briefings, particularly when it conflicted with his pre-existing opinions.

I think it's absolutely fundamental to understand what you know, what you don't know and what you might not even know you don't know. And in these cases, it's incredibly important to seek out experts who have put in the time and effort to figure out nuanced, thoughtful approaches to situations and understand their guidance. Trump has explicitly rejected the premise of that, suggesting that there is no such thing as a credible expert and that any man's instinct is as good as the opinion of people whose life work may have been spent in these areas.

What does Trump appear to respect instead? Loyalty, partisanship and the commitment to jump when asked to.

**The takeaway is that America doesn't value expertise.**

#### Sacrifice and selflessness

Trump's campaign sales pitch largely consisted of the message that "everything in America is awful, and Trump alone can fix it." This is a pitch that resonates with folks who are having a hard time, irrespective of whether it's truthful or not, but it's also an incredibly self-centered one. It suggests that the solution to problems in America isn't the American political system, but one all-powerful leader who can come in and resolve issues by force of sheer will.

More importantly, Trump's campaign issues and the explicit "America First" sales pitch (with all of its historical overtones) are fundamentally indicative of an attitude of "yeah, all y'all other folks can solve your own problems." Are you South Korea, worried about a belligerent Kim-Jong Un? Nice, good luck, maybe you should get some nukes. Are you a NATO member concerned about whether the U.S. will respect its treaty obligations? Better make sure you're putting in as many of your own resources as possible first and hope Trump notices.

This is indicative of a broader culture of unquestioningly prioritizing Americans over everyone else, when the thing that has made the U.S. so remarkable over the decades is precisely the opposite. When the U.S. invested billions into Europe as part of the Marshall plan, they were prioritizing the creation of a progressive global world order over investing in their own citizenry. When the US made a commitment to defend the Pacific nations from the threat of nuclear war, they were prioritizing nuclear non-proliferation over expanding the risk of nuclear war.

The Trump campaign, though, has made it explicit that the calculus has become simpler and more transactional. It's what have you done for America lately, and it doesn't matter how important anything is for the world. Whatever spirit of sacrificing for the good of the world's population the U.S. once possessed is now gone.

**The takeaway is that America doesn't value the greater good.**

#### Multi-culturalism

Finally and most importantly, the Trump campaign has been the most astonishing repudiation of multi-culturalism that I could have imagined.

When I was in Moldova, I spent a few months living with an AIESECer who had spent a few months on a work-and-travel job in the U.S., and he helped me crystallize the number one thing I admire about the United States: the fact that, unlike nearly every other country in the world, a person who was born to non-American parents could call themselves an American.

This is a profound statement, because it suggests that being American is an idea, not a birthright: it means that if you come into the United States, work hard and embrace the American ideal, you have just as much right to consider yourself an American as someone who (as Ann Coulter suggests) has four American grandparents. It's also astonishingly rare.

The Trump campaign, though, has marginalized this idea to the point that its survival is actively in question. The attitude that it's propagated is that Americans are special, unique snowflakes who deserve to have the world at their feet, and it's only the white, middle class Americans are the only people who really deserve to count as such. The othering of Americans has gotten so extreme that even people who were born here are getting attacked by the newly-energized alt-right.

That's to say nothing about non-immigrants and working to understand the cultures of other nations. If the U.S. can't even be bothered to try to understand the people who have come here and are striving to integrate, how can we expect Americans to empathize with people they've never met and whose cultures they've never experienced?

**The takeaway is that America doesn't value the perspectives of others.**

Of course, Trump's campaign repeatedly rejected basic human decency as well, and that's suggestive of perhaps the saddest thing America doesn't value.

### Where do we go from here?

I was raised to believe that the five things I called out above are fundamental to living, leading and getting ahead in our modern world. And yesterday, I found out that maybe they aren't as fundamental to this nation as I anticipated.

I can't say I'm completely surprised given what we've seen elsewhere in the world with the rise of nationalist parties, Brexit, and the move toward long-term authoritarian leaders. But I am still reeling because I genuinely thought these ideals would prevail simply because Trump was so aggressively irresponsible that the American people would be too smart to back him.

My hope was that we'd have eight years of a Clinton administration to figure out the answers to how we adapt and package these ideals to sell them in today's more cynical world. But we didn't have the breathing room I anticipated.

And now, to be frank, it's hard to imagine what comes next. There's no reason to believe we aren't in for eight years of Trump, given that after everything we learned about Trump over the course of the past year and a half of campaigning he was still embraced by nearly half the country's voters. In light of that, I can't envision what horrible thing a President Trump would have to do that would actually get his supporters to vote for someone else.

But either way, knowing nothing about how he will run his White House means we know very little about what will happen under Trump. The [reports of how he might go about achieving his campaign promises][3] are deeply concerning, but they can't take into account the random variables of Trump's unpredictability.

More important even than what he does in office, though, is what it does to the political system in the U.S. and in the world as a whole. And it's here that we have the real crux of the issue: **Trump's election, in conflict with the basic values I outlined above, along with other votes like Brexit around the world, is suggestive of the end of a world order we've relied on since the end of the second World War.**

That world suggested that an ever-closer cooperation between nations was desirable, that the free movement of goods, people and ideas was going to help improve lives and provide opportunities around the world, and that wealthier countries had an obligation to partner with poorer ones to improve the lives of their citizens, even, at times, at the expense of their own residents.

The new world, though, is every-nation-for-itself, every-person-for-themselves. It suggests that there's enormous value in taking care of your own, and damn everybody else. Every other time we've seen this nationalism rear its head around the world has ended in massive, prolonged worldwide crisis, and there are [signs that we are entering another loop of this pattern][4].

### As for me...

I'm lucky enough to have the luxury of having many months of travel ahead of me, and even though Donald Trump's election will likely make my trip substantially more expensive by dropping the value of the dollar and will make it more difficult for me to get a job when I'm back, I have a long time to figure out my answers.

But in the meantime, I have to deal with the fact that Americans have repudiated my ideas of what a leader&mdash;and in many senses, a decent human being&mdash;should be.

In the past, when I've found myself in situations where my values clearly did not match those of the people around me, whether because my values had evolved or because I didn't realize the values of a group until some event caused them to be demonstrated, I've simply been able to decamp and find a different place for myself.

But changing countries is more complicated than changing friend groups or teams at work, and besides, even though the U.S. is particularly sad because in many respects it has fallen the furthest in the least amount of time, the world as a whole shows many signs of the same malaise.

So what do we do now? We brace ourselves and we try to figure out how and if we can re-brand globalism in a way that makes it acceptable to the people who have embraced these "me and my country first" ideals. The good news is that many of these voters are really just looking for someone to acknowledge their frustration. The bad news is that it's not clear if we have any way to answer it while still advocating for the globalist ideas we cherish.

We have to find a way to fix the unfixable: to change the minds of people who not only don't want their minds to be changed but don't even pay attention to the means and places where we persuade one another. To change the minds of people who have demonstrated that preparation, honesty and expertise are just not that important to them. People who have decided we can set the greater good aside in favor of their good. People who aren't ready or willing to work to understand the perspectives of others. We have to find a way to pull the world back from the brink, to find a way to improve the lives of the angry so they can accept **our modern society is a Damn Good Place**.

And in the meantime, we have to quietly prepare ourselves for what we will do if we fail. But before all that, we have to get through the next few years together, supporting one another: because things will get worse before we can start making them better.

[^1]: I am, of course, an immigrant, but we all know that when Trump rails against immigration he's not talking about people who look like me. Sigh.

[1]: http://www.theatlantic.com/politics/archive/2016/11/a-trump-presidency-bracing-for-the-unknown/507047/ "A Trump Presidency: Bracing for the Unknown"
[2]: https://www.buzzfeed.com/craigsilverman/how-macedonia-became-a-global-hub-for-pro-trump-misinfo "How Teens In The Balkans Are Duping Trump Supporters With Fake News"
[3]: http://www.newyorker.com/magazine/2016/09/26/president-trumps-first-term/amp#success=true&return "PRESIDENT TRUMP: What would he do?"
[4]: https://medium.com/@theonlytoby/history-tells-us-what-will-happen-next-with-brexit-trump-a3fefd154714 "History tells us what may happen next with Brexit & Trump"
