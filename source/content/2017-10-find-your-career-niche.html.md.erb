---
title: It's hard out there for a generalist
subtitle: Want to get the best opportunities? Better commit to a specialty
date: 2017-10-20
category: Career
tags: specialization, money, decisions
---

Every single day, the news overflows with doom and gloom: politics is a disaster, people are at war, the gap between the rich and the poor is higher than ever, and everything is just generally awful. It's easy to fall into the trap of thinking that the world is going to hell in a handbasket. And yet [reality is very different][optimism]: fewer people live in poverty than ever before, more and more people have the ability to benefit from the modern world's conveniences, and many seemingly-intractable problems are being solved.

Modern life is quietly the best it's ever been for many people, and yet there is in fact something we're losing: breadth of knowledge and ability.

History is full of praise for the "Renaissance men," who painted, wrote poetry, read all the books, studied the sciences and had tremendous impact on their societies, but these men (and similarly gifted women who, for many complex and often unjust reasons, are not as well-remembered in history) demonstrate just one aspect of the generalist knowledge that abounded in historical society: that of the comfortably well-off. 

Lots of people had a broad set of skills, though: societies were smaller, and while towns had the butcher, the cobbler and the tailor, each person was capable of a wide range of tasks and possessed a surprising variety of knowledge. Most importantly, *as a percentage of the sum of skills and knowledge that existed at the time*, folks generally knew quite a bit, even if they only knew the basics.

How things have changed!

READMORE

### The super-specialization of modern life

If you want to find the closest thing to yesteryear's renaissance men today, you have to find people living an incredibly antiquated lifestyle: homesteaders, isolated tribesmen, and poor rural farmers. And even then, the things they're capable of can ensure basic survival, but don't represent nearly the percentage of knowledge historical renaissance men enjoyed.

The first 20-30 years of modern life, on the other hand, are really a progression of increasing specialization: 

1. Everyone starts off going to elementary or primary school, learning generally the same things.
2. By high school, people may have the opportunity to take a few electives, and there may be different levels of classes that go into varying levels of depth on the same subjects. Still, everyone ultimately graduates with similar degrees that imply a certain amount of general knowledge.
3. College undergraduates pick a major, but still have a set of core classes, and their major education generally gives the broadest possible view of the relevant field.
4. At the graduate level, assuming you pursue that path, students become quite specialized in a particular facet of their field, ultimately culminating in a dissertation that represents novel research in that area.

Beyond graduation, as students begin a career, their specialization only grows: not only are they focused on a specific field, but they’re learning the particular tools their company uses. They’re becoming area experts not just in the focus area of their company as a whole but in that of their department. Ultimately, they’re building a resume that qualifies them very specifically for some class of jobs and disqualifies them from others.

By the time a person gets through ten years of their career, they may be one of a few hundred people with "[a very particular set of skills][takenmovie]."

### Choosing is hard

The problem with this progression of specialization is that choosing what to specialize in can be incredibly difficult. Even choosing a major in college is for many students a major challenge[^1]: few are confident in their first choice, and at some universities, [upwards of half switch before graduation][nytmajors]. And that’s with just a few dozen options: the vast number of options to specialize people have in their careers is absolutely terrifying by comparison.

![Choosing is hard][picChoose]

A big part of the reticence to choose is the [fear of missing out (FOMO)][fomoexplainer]: the concern that by choosing to do something, you’re not going to be able to do a bunch of other things that (obviously) must be much more interesting and exciting.

Just as important is the fear that you’ll get pigeonholed in a field which may wax and wane in relevance. Like an actor that gets typecast for a character trope that is only popular once every few decades, your skillset may not stay relevant. If that happens and you’re not positioned to continue evolving, you could be in trouble: there may have been countless COBOL programmers once upon a time, but that skillset isn’t exactly in demand any longer.

And yet there’s incredible value in getting as specialized as possible:

- Most primitively, **specialization is lucrative financially**. Plenty of folks specialized in high-value businesses can tell you about the [many thousands of dollars they charge as their weekly consulting rate][consultingfees]. Indeed, specialization is the core value proposition of consulting: you sell your services on the premise that your skills are difficult to impossible for a company to otherwise acquire in a reasonable amount of time. 
- **Knowledge begets more knowledge**: specialization in something can put you on the bleeding edge of your field, and that makes it more likely that you'll somehow improve the state of the art. The people who make new things possible are most often the ones who've been thinking about that thing the longest.
- Finally, if you are world class at something uncommon, **unexpected doors will open to you**. When I worked at DreamLab, an advertising agency in Kazakhstan, we flew an American body paint artist all the way to Kazakhstan to participate in a series of ads for us because he was a true expert. Maybe getting paid to travel to Central Asia isn't your idea of an amazing opportunity, but the broader point is that you can't predict the opportunities you'll get in advance; you just know they'll expose you to interesting people and situations.

### Finding the niche

The punchline is this: with a few exceptions (e.g. a startup where you have to wear many hats simply because there's no one else to do so), being a generalist will not get you paid.

So, how do you actually commit to a niche? I’m still working through this myself, so take this advice with a grain of salt; still, here are my few thoughts:

#### Start with what you’re already voluntarily doing

You probably have a hobby or two: wood working, baseball, watching TV, etc., that you do not because it's lucrative but because you enjoy it. Of course not every hobby is going to help you specialize: even in that list you have to go deeper. After all, the more niche that hobby is, the more likely it is that you know significantly more about it than 99% of the world's population. For example, if your hobby is watching TV, well, that just gets you good at random pop culture trivia, but if you are one of the top ten fans of Parts Unknown, you likely have insights that few others would, and that might lead to opportunities.  

The problem, of course, is that many hobbies don't have a clear translation from hobby to opportunity. And all opportunities inevitably require significant effort. But if you're going to put a ton of work into something, why not start with something you like?

#### Do to become 

In many ways, the best way to become a specialist in something is by accident: you spend a lot of time working on a particular area, and before you know it, suddenly you're an expert.

As Josh Kaufman points out, [the key is to put in the time and do the things someone in the position you want does][dotobecome]. So if you want to become a writer, you have to write, write, and then write some more. After all, skills, just like your abs, are a muscle you can build. And as you build a set of skills you learn about yourself, too: maybe you're not cut out to be a fiction writer, but you can write marketing copy like nobody's business.

You don't have to stumble into a niche, though. You can identify a rough area of interest, and put time toward a number of related but distinct activities in that area. Eventually, something is likely to resonate. 

For example, maybe you want to build a cool iPhone app. You struggle through putting together the interface, but you blaze through wiring it up to analytics and learn to collect copious amounts of data about how your users are interacting with that app. Good news: you can specialize in helping other app creators understand their users, a highly lucrative niche. And you'd never have gotten there if you didn't start by doing: going through the tough slog of putting your own app together end-to-end.

#### Take credit

The thing that separates recognized experts in a field from a dozen other people with similar knowledge and ability is just that: recognition. Establishing a niche often requires going out on a limb and actually proclaiming yourself as an expert in something.

For those of us who struggle with imposter syndrome, publicly claiming expertise can feel more like walking the plank than going out on a limb, and there's an even bigger leap to be made when asking someone to pay for your expertise. But doors don't open themselves, and informing people about your capabilities is like turning the knob. 

So take credit for your skills, friends and readers. Sign up for the opportunities you're not yet sure you're qualified for. Do your best work when those opportunities give you the chance to shine. Before too long, you'll start gaining recognition, a little bit at a time. You'll know you've locked down a niche when other people defer to your expertise. 

*Photo by [Edu Grande on Unsplash][photocredit].*

[^1]: I know, I know. I can never resist the awful puns. Forgive me?

[optimism]: https://www.gatesnotes.com/2017-Annual-Letter "Bill and Melinda Gates Foundation - Annual Letter 2017"
[takenmovie]: http://www.imdb.com/title/tt0936501/ "Taken (2008) - IMDB"
[nytmajors]: http://www.nytimes.com/2012/11/04/education/edlife/choosing-one-college-major-out-of-hundreds.html?_r=0 "Major Decisions - The New York Times"
[fomoexplainer]: http://www.artofmanliness.com/2013/10/21/fighting-fomo-4-questions-that-will-crush-the-fear-of-missing-out/ "Fighting FOMO: 4 Questions That Will Crush the Fear of Missing Out"
[consultingfees]: http://www.kalzumeus.com/2015/05/01/talking-about-money/ "Talking about Money - Kalzumeus Software"
[dotobecome]: https://joshkaufman.net/do-to-become/ "Do to Become - Josh Kaufman"
[photocredit]: https://unsplash.com/photos/0vY082Un2pk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText "Photo by Edu Grande on Unsplash"

[picChoose]: <%= root_url %>/2017/10/find-your-career-niche/choose.jpg