---
title: The cool new thing
subtitle: How a Microsoft-using contrarian ended up with a Nexus 5 and a Mac
date: 2014-11-08
category: Technology
tags: software, pm, decisions
---

It's time for me to confess a deep, dark secret: when it comes to software, I can sometimes be a bit of a hipster. 

When I was in high school and into college, I frequently went out of my way to use different software just for the sake of not using the typical apps my friends were using. In the world of IM, I might have used ICQ, AIM and MSN as the protocols, but it never took me long to jump away from the official apps: over the years I used Miranda, Pidgin, Trillian, etc. much more often than actual MSN Messenger, AOL Instant Messenger and ICQ clients. I spent nearly a year using MEPIS Linux as my primary OS on a mildly-antiquated laptop. And of course, I spent about a decade using Opera as my main browser, a fact that got me consistently mocked at Microsoft.

Towards the end of college and especially when I started my job at Microsoft, though, I found myself jumping full force in the opposite direction, going out of my way to use the official Microsoft apps like Photo Gallery, OneNote and of course OneDrive. (I still used Opera until they decided to give up work on their own rendering engine and switch to Chromium.) I also bought in to the Microsoft ecosystem, using Windows Phone as my primary OS and an assortment of Windows-based PCs--mostly Thinkpads--over the years.

Ironically, sticking to "the man" during that period was actually somewhat hipster in its own way: as Apple gained success with iPhones and iPads and Android began its spectacular growth, most people weren't buying Microsoft-based devices, and as a consequence weren't having the same computing experience I was. So in spite of my convergence to the safe, no-one-got-fired-for-buying choices, I was yet again in the minority.

On the eve of my five year anniversary at Microsoft, I found myself increasingly out of touch with what most of my peers were experiencing in their day-to-day computing life. I had never owned an iPhone (though I'd had the occasional experience with iOS through iPods and iPads), and I hadn't used an Android phone since the very early, very painful stages of that OS. I hadn't used Mac OS on a primary computer in about five years. Instead, I was quite familiar with Windows, Windows Phone, and--all hipster-like--Palm's tragically doomed WebOS.

READMORE

### Blind spots

While in many respects Windows Phone, WebOS and the new Windows versions are all great OSes, sticking almost exclusively to them left me with a series of gaps in my awareness of "the new hotness." In terms of innovative OS features, these are all on par or even ahead of the dominant platforms. Rather, as you might imagine, the single biggest weakness of all of these ecosystems is app support, but that weakness isn't necessarily what you might expect:

* While pretty much all of the major apps are present, they tend to get new features later and be updated less frequently. The result is that often you can't actually use what people are excited about at that moment in time.
* New apps pretty much never come to your ecosystem first. When something suddenly blows up (e.g. Yo. Or maybe a useful app.) you can't usually jump to the app store and check it out. This means you can't knowledgeably weigh in on trends and developments as they happen.
* The presence of the second tier of apps is often weak. Hal Berenson wrote a [tremendously insightful critique of how to evaluate an app store][1] last year, and this "3rd-10th most popular app in the category" space is where all third platforms have forever struggled. I should note that Windows has actually improved in this regard pretty substantially since that post was written and Hal's numbers are almost all outdated--but it's still nowhere near Apple or Google's ecosystem. 

Just as importantly, by sticking to these less common platforms you don't experience the pain or enjoyment that users of these platforms do, and so when people come up with solutions to those pains, you might not understand why those problems are real. I was mystified over the excitement about alternative email clients on Android until I found out how abominable Google's native email client on Android is for myself.

### Going Android on mobile

All of this weighed on my mind as my Lumia 920 started acting up and was ultimately cannibalized to fix my fiance's broken screen. I wasn't excited about any other Windows phones on the market at the time, and so I thought, what the heck? What better time to try out something new? So after a bunch of review-reading and ebay-browsing, I bought a Nexus 5 and dove into stock Android.

Android's been a fascinating experience, because it's grown immeasurably better since I last tried it in the olden days. The apps have in most regards caught up to Apple's, and Google's relaxed approach to app store approvals mean apps get updated even more frequently than on iOS. The OS itself runs smoother, crashes less often, and has introduced a bunch of new features that actually make a huge difference to the usability of the OS--ironically, many of them cribbed directly from WebOS. (I suppose I shouldn't be surprised given Matias Duarte worked on WebOS before he took over Android design.) And yet, my impression is that Android is still a pretty frustrating experience overall.

It's entirely possible that my issues with Android are insurmountable, not because Google is incapable of fixing bad UX decisions, but because what I've come to realize is that years later, I still disagree with many of Android's decisions philosophically. Perhaps it's strange for a Windows power user to complain about similar decisions on Android, but I generally find that the flexibility I appreciate on a full PC is extraneous on a mobile device, and Android's shaky implementation doesn't help persuade me otherwise. 

Take intents as one example: Clicking on an Instagram link in the Twitter app always seems to go back to asking me whether I want to open it in Chrome or in the Instagram app, no matter how many times I pick one and tell it not to ask me again. I think intents are a good idea conceptually, but I think Android uses them in far more situations than are merited. I don't want to be forced into that decision every time (or the first time)--I want to be able to change my default apps in the settings when I go out of my way to do so. And if all I'm doing is clicking on a link, please please just open the thing in my web browser.

This is just one example, but it's pervasive throughout the OS: you pretty much have to replace stock components of the OS to get a positive experience, and if you don't, you have to put up with what can often be a clunky mess. It's come a remarkably long way, but I find myself still struggling to understand how normal users are content with their current experience. I suspect they have accepted Android's shortcomings in exchange for price, screen size or both, and I can't wait to see how the iPhone 6 does in the U.S. market in taking over the frustrated group of people who went with Android for the latter.

### Going Mac
It's been a pretty transformative computing year as a whole for me, and the next thing I wanted to do was buy a new laptop. I've spent almost a decade using nothing but IBM (and then Lenovo) Thinkpads, but I already had a Thinkpad X1 Carbon Touch for work, and I couldn't justify the price of another one for home use. I ultimately looked at just about every ultrabook in the $700-1000 price range, and my quest reached an entirely unexpected conclusion: I bought a MacBook Air.

I didn't set out on this exercise with the intent of switching away from Windows. Rather, I was persuaded by two major considerations, both of which I found somewhat surprising.

1. The Apple tax is no more. As recently as a few years ago, you could consistently get Windows laptops with comparable specs and build quality at a 10-20% discount to the equivalent Macs. (You could also get Windows laptops with comparable specs and garbage fit and finish at a 40-50% discount, but I've never found that category to be particularly interesting.) This isn't necessarily because the MSRP of Windows laptops is lower, but because they see much larger sales and more discounting than Macs do. For whatever reason, that doesn't seem to be the case for the MacBook Air: comparable Windows laptops, including the Surface Pro 3, are several hundred dollars more expensive, especially considering the latest sales on each. (That said, this was around the back-to-school season, when the Surface Pro 3 was still relatively new and the Macbook was already relatively old. I'd expect prices to equalize for the holidays.)
2. The popularity of the iOS ecosystem has created a boom in innovative app design on the Mac. Developers are reimagining major categories like email (Mailbox), building creative light-weight utility apps (Day-O), and generally partying on the Mac. Much like with Windows desktop apps, there's a lot of mediocrity (though on the Mac developers often charge surprising sums of money for said mediocrity), but there are a ton of learning opportunities from the apps that actually work.
	 
Ultimately, I found myself buying the MacBook because it offered the best combination of bang for the buck and apps I wanted to try out and learn from.

### Where to next?

The fact that I chose to buy two devices outside of my comfort zone is by no means a mea culpa for the Windows and Windows Phone ecosystems: not for me personally, and certainly not in general. I'm pretty confident I'll be back on Windows Phone before too long, and of course I still use Windows every day at work and on my home desktop. There are many things these platforms just do flat out better, even beyond my familiarity with them.

Rather, what I've learned is that it's incredibly dangerous to lose touch with the breadth of today's technology space. As a program or product manager, my job relies in part on the ability to make good judgments about the evolution of technology and extrapolate from those judgments to evolve your product in a thoughtful, positive way. With 20/20 hindsight, I feel embarrassed that I didn't spend more time and effort on broadening my technological horizons earlier, and I can pretty clearly envision work I might have done differently if I had. 

Ultimately, I think it's more valuable and important to try new things in general than what specific new things you're trying. 

[1]: http://hal2020.com/2013/05/13/the-windows-phone-app-problem/ "The Windows Phone app problem"