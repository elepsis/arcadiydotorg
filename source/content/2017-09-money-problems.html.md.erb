---
title: More money, more problems
subtitle: The complete guide to money while traveling
date: 2017-09-27
category: Travel
tags: tourism, money
---

Over the past year of travels, my wife and I have acquired and spent all sorts of different currencies: everything from the British Pound Sterling and the Euro to the Lao Kip and the Bosnian Convertible Mark.[^1]

We hardly ever carried cash back home, so it's been a big life change to have to pay for things using paper money a majority of the time. The currencies changing regularly as we hop from country to country, along with the need to pay for most things using cash, means we're needing to acquire it almost constantly. 

![Foreign currency and coins - by Philip Brewer][pic1]

We've gotten a handful of questions from folks about how we go about dealing with foreign cash[^2]:

- How do you get cash while abroad?
- Are there any gotchas to be aware of?
- How much local money do you get at a time?

Cash is the trickiest aspect of dealing with money while travelling, but it's just one part. The other is to make sure you're using the right credit card(s): ones that won't waste money on every purchase you make, and that provide valuable benefits while traveling. Here's what we've learned about both cash and credit cards over our 25-country jaunt.

READMORE

### Getting cash: Embrace the ATM

The way we always get cash in foreign countries is by using local ATMs: we have a lovely, free-ATMs-worldwide debit card, so we don't hesitate to hit up a machine when we need it.

#### Why ATMs?

Many people still think that the best way to be sure of getting money in a foreign country is to show up with a wad of USDs and take them to a currency exchange counter somewhere in your arrival city. Many of them will also supplement by trying to acquire local currency in advance, back in their country of origin...which works fine when you're trying to get Euro or Yen, but not so well when you're looking for something more esoteric like Vietnamese Dong.

The good news is that this is, indeed, quite likely to work: there are currency exchange counters at nearly every international airport and in almost all tourist centers worldwide. The bad news is that it's not at all likely to work out in your favor financially: these currency exchange counters have to pay for some pretty expensive real estate, and they do that by profiteering off needy tourists in a few different ways:

- Commission: Many cash exchange places charge commission on top of whatever rate they exchange your money at, which may be a flat fee or a percentage of your transaction. Particularly when you're changing small amounts of money, this can easily eat up several percent of your cash.
- Awful exchange rates: Even if you find a cash exchange counter that doesn't charge commission, they'll make up for it by offering a worse exchange rate. At a good exchange place that's frequented by locals, you'll get hit to the tune of 1-2% over the bank rate. At many tourist traps, though, you can be looking at a 5% or greater markup.

Moreover, needing to change your money means you have to decide in advance how much money you are going to need, and carry around a bunch of cash as you travel (or keep it locked away in your hotel, where you can't conveniently get it if you need to). There's a reason we don't operate this way at home, and if you're able to confidently use ATMs abroad, you don't have to do it there, either.

If you can use an ATM whenever you need, you can just withdraw a reasonable amount of cash each time, and the exchange rate provided at an ATM is much, much closer to the official exchange rate than you can get on the street.[^3]

#### The ATM tradeoff

The problem with using ATMs while abroad for most people is simple: fees!

Even back home, we're all used to having to find **our** bank's ATM in order to avoid getting charged an outrageous fee, oftentimes both by the bank who deigned to allow us to use their ATM and by our own bank for the privilege of accessing our own money. I think this is entirely absurd in this day and age, but most people still put up with it without complaint.

Now consider what happens when you're abroad.

First off, with few exceptions, you're going to have a hard time finding your local bank's ATMs in most of the countries you're likely to visit. So you have to assume you're going to use another bank's ATM, and you'll likely have to pay a fee for that. On top of that, most banks charge an additional fee for accessing your account from a foreign country.[^4] *Then*, once they're done gouging you for using an ATM, they'll charge you a percentage of your withdrawal for the privilege of performing a transaction in a foreign currency.

Before you know it, your withdrawal of, say, $100 might cost you $100 + $4 ATM fee + $5 foreign transaction fee + $3 for a 3% currency conversion fee = $112! If that's your life, for the love of god go to a currency exchange counter, because even they aren't brazen enough to shoot for a 12% markup. Or, you know, you could switch to a financial institution that isn't out to screw you in every possible way.

#### How we do it

Pretty much all long term travelers recommend the same checking account to Americans who intend to travel frequently: [Charles Schwab Bank's High Yield Investor Checking account][schwab]. It stands head and shoulders above any other checking account option for travel, and its ATM policy is what makes it so appealing.

That policy is simple: 100% free ATM withdrawals, anywhere in the world. Period.

It's the only bank I've ever been able to find that puts no real asterisks on that statement. Free withdrawals means:
	
- Schwab will never charge you for using an ATM, no matter which ATM you use.
- If the bank whose ATM you used charges you a fee for using it, Schwab will refund it automatically at the end of the month. That even includes absurd Vegas casino ATM fees!
- No Visa network currency conversion fee of 1%, which even most other "fee-free" ATM banks charge. Looking at you, Fidelity.
- No special account status needed to get the benefit. Some banks waive some or all of their standard fees for customers who have many thousands of dollars in accounts with them, but that's not for everyone and a pain to keep track of.

In a few particularly cash-heavy months of travel in southeast Asia, where many ATMs charge $4-5 every time you withdraw any money, I got refunds of $30-50 each month.

We find that the exchange rate we get from it at an ATM is a better deal than any exchange office pretty much 100% of the time. In fact, it's been years since we've traveled to another country with any local currency in tow, though we do tend to keep a few crisp US bills on hand in case of emergencies.

The fact that it's far and away the single best checking account for travelers is just the cherry on top of the fact that it's a great checking account overall, with no maintenance fees, no minimum balance requirements, free checks, etc., etc.

I've had my Schwab account for over a decade now, and to be honest I can't believe this offer has remained unchanged. It seemed like it was too good to be true in 2007, and it blows my mind that ten years later they still have this phenomenally generous policy. Anyone who's ever asked me about Schwab knows I'll sing their praises and recommend them to everyone, and I now have a decent chunk of change in assorted accounts with them, so I suppose you can view their checking accounts as a marketing expense. 

So our approach, and the one we recommend, is super simple:

1. Open a Charles Schwab account.
2. Use your Schwab debit card at ATMs whenever you need cash.

The only caveat is that you will occasionally run into an ATM that doesn't want to work with foreign bank cards. We've found that if you stick to ATMs run by the biggest local banks, whatever they may be, you'll usually be just fine.[^4] Finally, like with any other bank account or credit card, it's worth giving them a call before you leave on your trip to let them know you'll be traveling. That ensures there aren't any awkward moments when your ATM card is locked out because *why would you* be trying to get money in Romania?

### How much cash do I need?

Irrespective of **how** you acquire local currency, though, the dilemma that always arises is how much money you actually want to withdraw. In countries that use a common currency like the Euro, it's not really much of a dilemma, as any excess cash you withdraw will always come in handy, but what happens when you're in a country with its own unique currency for just a few days?

#### Who cares how much you withdraw?

As we've travelled the world, we've done it on a budget that I can only describe as "extremely fixed." We are not pinching our pennies, but there isn't a ton of flexibility for unnecessary extravagance. And there's nothing more faux-travagant than having money go down the drain, lost to exchange fees or the inability or impracticality of changing excess single-country currency at the last minute.

What this means when it comes to currency is that there are concrete financial consequences when we withdraw too much local cash from an ATM: either we end up exchanging any excess cash into another currency on the last day of our trip, usually at a location that isn't likely to supply the best exchange rates, or we just end up keeping the foreign currency and effectively eating the loss. That's to say nothing of the mental stress of having to deal with that last minute task when you're preparing to leave a place. When we left Macedonia, we ended up with the equivalent of about $50 in Macedonian Denar we couldn't exchange because we couldn't find a chance to exchange it the day before, and our flight out was so early in the morning. $50 is a good chunk of a day's budget during our travels. 

The alternative, of course, is to withdraw too little cash and find yourself hard up in a situation where you need it. Oftentimes that just means you have to find the nearest ATM, and that's usually easy enough, but when you're in a taxi and it's late and you just need to get home, trying to explain to a driver who doesn't speak your language that he needs to find you a bank ATM before you can pay him is a special kind of nightmare.

So you have to strike a balance between withdrawing too much and too little. But how?

#### What do you know about where you're at?

The best way to figure out how much cash you're going to need is to have some understanding of the place you're visiting. Most important to think about:

- How much do you expect to spend per day? Once you have some idea, likely based on the average cost of things in a country, you'll know roughly how much you need to withdraw to cover a few days of spending. And you'll be able to plan for the next time you need to visit an ATM.
- Which of your expenses can be paid via credit card or were pre-paid? If you're staying at nicer or chain hotels, and eating at fancier restaurants, it's possible you can use your credit card for the vast majority of your expenses, and your cash needs will drop dramatically. Some countries are also much more credit card-oriented than others; in Scandinavia some places are actually no longer accepting cash at all.

#### Don't fear multiple withdrawals

Many people have been trained by ATM fees and mark-ups to make few large withdrawals, especially while traveling. They'll go to the ATM (or exchange) once while they arrive and try to get enough money for the entire trip.

When you're in the brave new world of fee-free ATM usage, you don't have to take that approach: you can just withdraw the appropriate amount of cash more or less on demand, just like you would at home. That allows you to minimize the risk that you'll be left with more cash than you need **and** means you don't need one of those goofy money belts to hide your excess piles of cash.

Of course, sometimes you do end up with significant amounts of cash, such as having to withdraw a bunch to pay for a cash-only hotel stay. But we generally try to do that at the last possible moment, bearing in mind daily withdrawal limits and ATM availability.

### Pick the right credit card (or five)

Figuring out the right way to acquire cash while abroad is much more important than figuring out the right credit card for travel, because there are many more ways to lose money while dealing with cash, and more of it at that, than with a credit card. But once you've gotten your cash situation straightened out, figuring out an appropriate travel card strategy is the obvious next step.

#### Debit versus credit

I think that most anyone with any semblance of self-control should use credit cards in general, but this is especially true when you are abroad. If your card (or just the card number) gets stolen while you're abroad and a thief decides to go on a spending spree, wouldn't you rather they were spending the bank's money rather than your own?

Your debit card is also the key to getting cash while traveling, so it's doubly risky to hand it out everywhere because of how painful fraud would be while you're in a foreign country: you'd be stuck with no card **and** no cash. Finally, I'm not aware of a single debit card that doesn't charge fees for being used abroad, so it's not even a wise financial decision. So if you're going to be paying for anything with a card while travelling, make sure it is a credit card.

#### What do you want in a travel credit card?

The first and most obvious thing you should look for in a travel credit card is *no foreign transaction fees*. Many credit cards, and particularly the no-annual-fee ones most people get from their banks, will charge a three percent fee on top of any transaction you perform in another country. The foreign transaction fee can creep up on people in unexpected ways, too. For instance, if you book a flight on a foreign airline's website, you may well get charged a foreign transaction fee, even if they happily quoted you a price in dollars and you're booking it from your own home. 

This is just money shipped down the drain, and there's absolutely no reason to pay it. Thankfully, **most** American credit cards that do require an annual fee no longer charge fees for foreign transactions, and [there are a handful of no-annual-fee credit cards that don't charge these fees at all][noftfccs]. 

Second, and with apologies to American Express: *your travel credit card needs to be a Visa or MasterCard*. We've traveled through most of Europe and Asia, and these are the only two card networks you can actually **rely** on to be accepted just about everywhere. If you only have an American Express or a Discover, you may as well assume you don't have a credit card.[^5]

Finally, it's worth thinking at some length about the so-called "fine print" benefits that your credit card provides. Everyone looks first at the headline benefits, like getting points for every dollar you spend or free checked bags on flights, but other high-end benefits are sometimes just as important. Many higher-end credit cards have excellent trip or baggage delay and/or loss protection provisions, for example, and will cover you if you need to buy clothing or accessories as the result of a travel problem during a trip you booked with that card. There are a bunch of other benefits under that same umbrella, and you should read and understand them.

#### Don't be afraid to pay an annual fee

Many people are extremely averse to paying annual fees on a credit card, and when we tell them that the credit card we use for most of our travel expenses has a whopping $450 annual fee, their eyes pop out of their heads. And yet we've actually **earned** money from this credit card this year, to the tune of several hundred dollars.

The trick is to figure out whether the benefits the card provides can offset the annual fee, and whether you'll be able to take enough advantage of those benefits. For example, there's a credit card with a $49 annual fee that in exchange grants a free hotel night at one of nearly 5,000 hotels in the world, ranging from budget to luxury. We'll have this card as long as the value proposition remains this good, even if some years we only use that benefit for a one-night staycation near home.

Moreover, paying an annual fee does tend to come with some significant quality of life improvements. You'll generally get better customer service from your card company, and your phone calls will be picked up more quickly. The suite of miscellaneous travel benefits will usually be stronger. And, as I mentioned earlier, you likely won't have to worry about foreign transaction fees.

Sometimes, you might think you'll get enough value from the card to keep it, but you just don't, and that's okay too: just make sure you run the numbers, and then close it the following year if things will stay the same. But don't shy away from a card just because you have to pay a fee for the privilege of using it.

#### Some other things to think about

There are a handful of other useful things to contemplate when you decide what credit card(s) to go with.

One thing we've encountered on our travels is that most of the world uses Chip and PIN technology for their cards, rather than America's Chip and Signature. This means that when you go to use your credit card, you not only insert your card into the reader rather than swiping, but you also enter a PIN. Most places are able to deal with American credit cards that then spit out a paper and demand a signature (though you might want to carry your own pen!), but all sorts of automated ticket or parking machines are not. We've encountered many a subway ticket machine where we wouldn't have been able to buy tickets without a Chip and PIN-compatible credit card (or appropriately small bills in cash).

Another really valuable benefit that some credit cards include is to serve as primary insurance on car rentals booked with that credit card. Almost all credit cards provide some car rental insurance, but it's usually a secondary insurance, which you would contact to cover costs beyond what your own, existing car insurance pays. A card that provides primary coverage, on the other hand, means you never need to call your own insurance company if something happens to your rental car: you just call your credit card service line, and they will deal with it.

There are also quite a few credit cards that provide specific benefits when flying a particular airline or staying at a particular hotel chain. These benefits range from the useless (woo-hoo, I get to board in zone 92 just ahead of the last two chumps who don't have some sort of priority boarding) to the compelling (a free checked bag for **everyone** on your itinerary, a 20-25% discount on in-flight purchases, etc.), but it can definitely be worth getting a credit card from an airline you expect to fly with or a hotel you expect to stay at with any regularity.

One final note: oftentimes when you're paying with a credit card in a foreign country, you'll be asked whether you want to pay for a transaction in the local currency or in your own. You should *always choose to pay in the local currency*. Not only is the currency conversion performed at an extortionate rate that will cost you a lot of money, but most banks still consider it a foreign transaction, so you won't even save on any kind of foreign transaction fee.

You can compare [another big list of no-foreign-transaction fee credit cards here][morenftccs]; I'm sure you can find one that meets your needs and matches the travel companies you use.

#### How we do it

We actually have...quite a few...credit cards; probably more than we need to keep at this point. We spent several years in the lead-up to our trip collecting hotel and airline points via credit card sign-up bonuses, and we've taken advantage of those to score some cheap stays and take most of our transoceanic flights in business class. But there are some cards that are the mainstays of our strategy, and these are the ones we'll cover here.

##### Citi ThankYou Prestige

This card is the mainstay of our credit card spending, and the one we've gotten the most value from over the past year. While it has a massive $450 annual fee, $250 of that is offset immediately with credits towards flights booked on the card, so at most the actual annual cost of the card is $200. 

This card has great travel protections and benefits, earns a healthy amount of points for any hotel or airline expenses, and gets us into Priority Pass lounges all around the world, saving us the cost of food and drinks at many of the airports we've been to. 

But the biggest benefit that's actually made us money this year is a complimentary fourth night hotel credit. You have to call them up to book hotels, but they do it through the hotel's own sites, so you get all the benefits of a direct booking, and then you just have to pay for your hotel with the card. That means we can't use the benefit for hotels that only take cash, and we mostly use it when we're staying at a hotel for exactly four nights (or if it's fairly expensive). But it's covered hotel nights that ranged from $30 to $300, saving us enormous sums and bringing significantly nicer hotels than we'd otherwise go for down into our price range.

##### Chase Sapphire Preferred

We've had this card for a while, and until we got the Citi card, it was our best card for travel expenses. Nowadays we keep it mostly because of its fantastic car rental coverage, where it serves as primary insurance.

It still provides a great value in terms of benefits for a relatively low annual fee, and it can quickly earn a large number of points in Chase's rewards program.

If we decide it's worth it, we may at some point upgrade to this card's newly-released big brother, the Chase Sapphire Reserve, and weigh its benefits versus those of the Citi Prestige above.

##### Barclaycard Arrival+

Barclaycard is one of the few American card issuers that provides cards that support Chip and PIN technology, so we keep this card around to use when signature cards just won't do the trick. The Arrival+ also simply provides at least 2 points per dollar spent to be redeemed for travel expenses.

We currently have both this card and their American Airlines Aviator Red credit card. We'll be getting rid of one of them this year, but haven't decided which.

##### Hotel cards
We also have two hotel credit cards: one from IHG and one from Hyatt. Both of these cards provide a free night each year at one of a large number of hotels, and their annual fee is well worth that benefit. 

### Spend more on your trip

The great thing about figuring out the right checking account and credit cards for your travel is that you can take all of the money you save on stupid fees, and put it towards getting that nicer meal, or visiting that museum you weren't sure was worth the money. After all, if you have the choice between spending more on an experience for yourself or on funding your bank CEO's latest yacht, don't you know which you'd prefer?

So get cracking: sign up for an account with Schwab, and get credit cards that will help you save money while travelling. You won't regret it. 

[^1]: Which, seriously, is defined by a 1:1 exchange rate with the former Deutschmark, which is a currency that no longer exists--so it's fixed at the Mark's original exchange rate to the Euro.
[^2]: The photo in this article is courtesy of [Philip Brewer][photoattrib] on Flickr (CC BY 2.0).
[^3]: There are a few countries, like Argentina, where the official exchange rate with the US Dollar is widely viewed as a joke, and you're better off exchanging your money "on the black market" at an unofficial exchange counter rather than using an ATM. But these are very much the exception.
[^4]: When I was young and stupid enough to use Bank of America, I learned that they will even charge you a "foreign transaction" fee for checking your account balance on a foreign ATM. I continue to hope they go out of business.
[^5]: One notable exception is Japan, where foreigners need to use ATMs inside the ubiquitous 7/11 stores or at Japanese post offices, not the big fancy banks.
[^6]: American Express does offer many very attractive credit cards you might consider as secondary options, and many of them have excellent benefits, but you can think about that for your second or third credit card.

[schwab]: http://www.schwab.com/public/schwab/banking_lending/checking_account "Schwab Bank High Yield Investor Checking Account"
[noftfccs]: https://www.nerdwallet.com/blog/top-credit-cards/no-foreign-transaction-fee-credit-card/ "NerdWallet: Best No Foreign Transaction Fee Credit Cards"
[morenftccs]: https://wallethub.com/credit-cards/no-foreign-transaction-fee/ "WalletHub: 2017's Best No Foreign Transaction Fee Credit Cards"
[photoattrib]: https://www.flickr.com/photos/bradipo/1435739708/ "Philip Brewer on Flickr"

[pic1]: <%= root_url %>/2017/09/money-problems/coins.jpg