---
title: Music on the web
subtitle: Why is YouTube so often the best option?
date: 2012-02-26
category: Technology
tags: design, law, music, web
---

Anyone who's ever traveled in my car will tell you that I have a pseudo-quirky taste in music. While many contrarian people listen to indie music for their *I'm a unique snowflake* fix, my preference is to listen to popular music produced in other countries. The result is that my music collection is an incredibly eclectic mix of languages: plenty of English and Russian, languages I actually understand, but also everything from French and Japanese to Kazakh and Indonesian.

You can probably imagine the struggles I have finding this range of music on modern subscription services like [Spotify][1] or [Zune Pass][2]. While music from the most popular French or Spanish artists is generally available, going just a few steps off the beaten path gives me either no matches or an album listing with a sad "unavailable" sign next to any albums by that artist. The situation in stores like iTunes and Amazon tends to be better, but I don't necessarily want to buy every song right away, and there's still quite a lot missing.

The utter lack of support for foreign music is frustrating because I know I can simply navigate to YouTube and find multiple videos that will do a perfectly adequate job of letting me listen to songs by most any artist I can think of.

I realize my musical tastes are hardly representative of most people's, of course. But everything I've said about foreign music applies to many indie musicians, particularly ones that may not yet have a deal with a notable record label.

### Content licensing is fun!
There's a simple reason for why YouTube has far and away the largest selection of music online: Unlike the subscription services, it doesn't need to acquire a content license before it will allow users to listen to a particular song (and view some sort of video). Rather, YouTube relies on two means of getting content:

* Major services that promote artists by uploading their music videos directly to YouTube, like VEVO
* Fans, listeners and creators upload videos of music they like on their own

[VEVO][3] and company cover the same general set of music pre-negotiated licensing tracks do, but it's the second method that gives YouTube's music collection its incredible breadth. And to keep things legal, Google has added lots of functionality for record labels to monetize music on YouTube, with the ability to display ads on videos that feature a song, block them in particular markets or to take them down entirely.

### And the collection is just the tip of the iceberg...
While YouTube is unrivaled in its collection, it's also got quite a few other things going for it. First and foremost, it's incredibly *low friction*: I don't need to sign up or log in to an account, launch an app, or wait for anything to download: I just open the site in my browser, type something in the search box, and before I know it, I'm listening to whatever it was I wanted to hear. Plus, I can instantly share a link to that video with all my friends, and be pretty confident that *they* will be able to hear the song with the same minimal amount of friction.

YouTube's existing functionality does a reasonable job of covering other key music site functionality, like the related videos section letting people learn about new, similar artists they might like. Over the past few years, YouTube has also added quite a few music-specific features, like artist-specific "YouTube Mixes" and upcoming concert information.

### YouTube isn't the solution
There's at least one difficult-to-solve problem that prevents YouTube from being the One True Music Service, though: the fact that it's a video site, and every musical track is tied to an only sometimes interesting clip.

This is fine when you're consuming music on a computer with a fast internet connection: just play the video in the background as you go about your other business. But so much of our music consumption happens in other contexts where the videos make YouTube an unlikely option. After all, no one wants to wait for a video to download on a slow mobile data connection when they aren't planning on paying attention to the video anyway. And that assumes people have a data connection at all. Playing videos also uses more battery life, and most modern mobile devices can't multitask with videos the same way they can with music.

Ultimately, unless YouTube one day decides to make audio a first-class citizen on its site, that limitation will continue to prevent it from being usable as a music service outside the house.

### And what of the music services?
There's no question that music services offer an experience that is more optimized for straight-up music, and features like upcoming concerts are either already available or easily added.

The collection question, though, is harder to fix, and it has mystified me for a long time. Given that YouTube is successful with a model where the legality of an upload is determined after the inclusion in its playable corpus, why hasn't a single music service emulated the model? Indeed, why hasn't [Google's own music service][4] gone down a path where any user-uploaded content is included in its database and playable by anyone else unless a record label desires otherwise?

Perhaps YouTube is truly just a historical accident that the record labels wish they could put back in the bottle. But shouldn't that historical accident  serve as precedent for the possibility of other sites that follow the same model? [SoundCloud][5] is one audio-focused site that relies on user-generated content, but it's explicitly not focusing on music.

Until someone builds that service, I will have to continue relying on YouTube for discovering and trying out artists, and assorted unlicensed means for actually acquiring music the industry hasn't bothered to make available to me.

[1]: http://spotify.com "Spotify"
[2]: http://music.xbox.com "Xbox Music"
[3]: http://vevo.com "VEVO"
[4]: http://music.google.com "Google Play Music"
[5]: http://www.soundcloud.com "SoundCloud"