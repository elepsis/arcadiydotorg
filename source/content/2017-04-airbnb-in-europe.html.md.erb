---
title: Staying at AirBnBs in Europe
subtitle: A brief guide to help you make the leap beyond hotels
date: 2017-04-21
category: Travel
tags: guides, lifestyle, trip
---

When we left the U.S. to go on our grand adventure last August, we had most of our first month of travels planned out, but almost no detail beyond that. During that first month we wanted to make sure to check out a wide variety of accommodations and figure out our minimum requirements for places we'd stay in. That way we would know how little (or how much) we would need to pay to meet our needs, and how to best find reasonable places. So in that first month, we stayed in a few hotels of varying quality, several bed-and-breakfasts, but also a hostel and a full-fledged flat through AirBnB. 

Now, more than six months into our travels, we've stayed at over a dozen AirBnBs throughout pretty much all of Europe, and we're now staying in some in Asia as well (I'm currently writing this from a nice apartment in central Ho Chi Minh City). 

I think many people are a little hesitant to stay at an AirBnB, especially in a foreign country, and we certainly were as well our first time. So here are some basic tips to help get you over the hump of getting a more local travel experience with an AirBnB.

![Try out AirBnB][picAirBnBHead]

READMORE

### Should you think about an AirBnB?

There are trips and places where using AirBnB makes sense and others where it might not. There are some trips where we start at AirBnB and others where we check it only briefly if at all. Here's when I look to AirBnB first:

#### Bigger cities

![Cities like Moscow have lots of AirBnB options][picAirBnBMap]

In some countries, particularly in Western Europe, AirBnB is actually quite widespread, but in most places, it's really only the bigger tourist destinations that offer ample choices. At least at this point, the further you go off the beaten track, the harder it is to find a place that will meet your needs. At the same time, the few places that are available might be real gems, so it's worth taking a quick glance no matter where you're going.

#### Longer trips

AirBnB will, of course, let you book trips of any length, from just a night to months. But we've found that both from a cost and happiness perspective, AirBnBs are a much better option when you spend a bit longer, ideally four or five nights and beyond.

There are a few factors to this. First off, AirBnB charges a service fee for each stay, and while that service fee varies with the duration of your stay, especially on short stays it can actually push up the total cost of your AirBnB to be competitive with a hotel. In addition, hosts on AirBnB quite frequently offer discounts for stays of over a week and especially a month.[^1]

Second, and particularly if you get a larger AirBnB that includes something like a kitchen, you might want to get groceries and cook some of your meals. It doesn't really make sense to buy much if you're staying for a short time, so you probably wouldn't get as much value from having access to a kitchen without it. If you're just in town for a few days, you'll likely want to eat out for most of your meals anyway.

Finally, and obviously, the real feeling of living like a local is difficult to appreciate in a short amount of time. If your itinerary leaves you needing to run around day-in and day-out and touristing, where you stay might just be a place to sleep, you might not get as much from an AirBnB.

#### More people

Hotel rooms scale well for large groups like conference attendees: people who don't know one another well enough to want to live together or need personal common areas to spend time together. On the other hand, they just don't scale super well for families or groups of close friends.

This is particularly challenging outside of the U.S., because while most American hotels will happily let you put up to four people in a single room, those abroad are much less well equipped to fit that many guests. Even if there are sufficient beds to fit four, the rooms might not be comfortable for that many people with suitcases.

AirBnB can provide a much wider set of accommodation options that will fit everybody, and oftentimes it will be cheaper, too. When we had a friend join us during our trip, we spent almost the entire time staying in AirBnBs that easily accommodated three, and though The Walls Were Thin between us, ensuring everyone heard everybody else's snoring, we were able to travel for a small fraction of the cost of two rooms at hotels, and had much more room to breathe. 

#### Space and feel of home

That breathing room is a big deal, especially when you're in a place for a while. Even when you only book a private room, many of the accommodations will allow access to some common areas within their house, and these are much more usable than what you might encounter in a hotel lobby. And of course, if you book an entire place, you may have all sorts of niceties, like a real kitchen or a couch/sitting area&mdash;like a fancy hotel suite but probably at a fraction of the price.

That extra space can make the difference between being able to hang out in your "local home" and feeling like you have to stay out, because it increases the number of activities you can comfortably do in your own, private space. Whether it's getting some photos processed or just staying out of your significant other's hair, you have the room to make it happen.

The feel of home is also an important aspect of an AirBnB stay. I've never had a hotel room feel like "home," but several of the flats we stayed at during our travels genuinely feel "ours" in some hard-to-define way. The ability to grow attached to a place might be an intangible benefit of staying at AirBnBs, but it is a real one.

#### Get to know a local
Staying at an AirBnB, particularly in a private room setting, gives you the opportunity to interact with your host and learn a little something about the local culture. We stayed at one in Aveiro, Portugal where the host lovingly restored his grandfather's home and art studio, and we got a tour of the house that let us see dozens of beautiful artworks and watch a short film his grandfather filmed decades ago in their super-impressive basement cinema. We got to spend an evening chatting with the hosts, learned about a talented artist we had never before heard of, and left with an infinitely more memorable experience than we could have gotten in a hotel.

### Think about the downsides

While I think AirBnB is a fantastic way to stay in many places, I want to be completely honest about the bad parts of staying in one. These may or may not be as important to you for a given trip, but make sure you are going into your trip with your eyes open.

#### No amenities, including cleaning

One of the nicest things about staying at a hotel on a vacation is the fact that your room gets taken care of by magical cleaning elves every day when you go out, and you arrive back to a room that looks almost as good as when you first checked in each evening.

With an AirBnB, you're not going to get any of that. Many hosts will offer a cleaning service for longer stays, but at an additional charge, and it certainly won't be daily. That's more than fine for us on most trips, but it is a big difference from your average hotel.

You're not going to get an onsite restaurant, included breakfast, and certainly no room service from an AirBnB, either. On the other hand, your host might be able to give you even better advice about cute local restaurants and places to hang out than you'll get from a fancy hotel's concierge.

#### Location

Many AirBnBs are a little further off from the main tourist areas and the city center. That will put you into neighborhoods with more local character and (usually) a good bit more peace and quiet, but it will mean that you'll have to make your way to the sights rather than them being right outside your door.

We actually find this delightful, as our ideal location in most towns is a 15-20 minute walk outside of the main tourist drag, but particularly if you are mobility-challenged or want to be surrounded by foreigner-friendly places, staying in a centrally-located hotel might be a better bet.

#### Communication with your host

Depending on what country you're going to and what languages you speak, there may be more of a language barrier between you and your host than between you and the front desk at a nice local hotel. We've never had this be a problem, and as long as the property has at least a few reviews written in English you can be pretty confident that you'll be fine, but if you have specific nuanced questions it might pose more of a problem.

#### Wider range of accommodations

Why would having more options be a bad thing, you ask? Well, we happen to think it isn't, but it's important to consider that we all have all sorts of ingrained expectations about a hotel that may or may not be true about an AirBnB. A trivial example: are you going to have soap provided in the bathroom? 99 percent of the time the answer is yes, but sometimes you're surprised because it's not there. At a hotel you'd just ask the front desk but at an AirBnB that's not an option.

There's a much larger variety of properties available on AirBnB and that means it's on you to be a little bit more thoughtful with your planning and more selective with where you stay.

#### Potential country legal issues

AirBnB may be in a little bit of a grey area legally in many places, particularly with small hosts not necessarily jumping through all the hoops to get listed as a "registered" accommodation. This is just as true in the U.S., where many cities are trying to crack down on unregulated rentals, as abroad, but in another country you likely won't be as comfortable with the law. This concern is usually easy enough to assuage with just a bit of research, but it's something worth keeping in mind.

### Choose your place wisely

Okay, so you're convinced that you want to go down the AirBnB path for your trip. You go to the website, do your search, and find twenty pages of options. How do you figure out which one to actually book?

#### Private room, shared room, entire place

![Make sure you choose the right type of room][picAirBnBRoomType]

This seems like a simple choice: do you want a personal room only&mdash;essentially the equivalent of a hotel room, just in someone else's house&mdash;or a whole apartment or house where you only interact with the host when you arrive and depart? Or are you okay with just a bed in a common area? 

In reality, though, we've seen a wide range of interpretations on what "entire place" versus "private room" should actually mean. We've stayed in "private rooms" that also allowed for shared access to a kitchen and ample common space, which gave us a rather apartment-like experience. We've also stayed in "entire places" where that was interpreted to mean "you have access to the whole apartment except this one room where the host may still sleep at night."

To be clear, none of these living situations came as a surprise to us: we knew going in what we were signing up for. But it did mean that it wasn't as simple as checking the right box in the AirBnB filters and assuming: we always search for *both* private room and entire place and see what that means. 

#### Amenities

![Double check the room features you care about][picAirBnBAmenities]

When I first started booking AirBnBs, especially in Western Europe, I honestly didn't even think to double check certain things I consider to be basics of any accommodation. All of that ended when we managed to book eight days at a flat in Berlin that didn't have wifi. Now, thankfully, we had high speed mobile data on our phones at the time, so it wasn't a complete disaster, but it was still a rude awakening about the need to do better due diligence.

Now, we filter on a few specific niceties being available: wireless internet, first and foremost; a hair dryer for my wife; and, in places where we are staying for a while, a washing machine. AirBnB's list of filters is actually quite extensive, and your priorities might be different from ours, so make sure you explore it and don't hesitate to apply the filters that will make the difference between an okay and a great stay for you.

#### Reviews

My number one frustration with the AirBnB site is the inability to filter listings by review score or especially the number of reviews. I understand why, from a platform perspective, AirBnB doesn't want to empower users to easily filter out new listings that might not have any reviews yet, but from my perspective as a guest, I'm not going to stay at a place that has zero reviews.

While when staying at hotels you have a relatively narrow standard deviation of what to expect for any given star rating, the variety of options you might encounter on AirBnB is in some ways both its best and worst feature: you're much more likely to have an unexpectedly wonderful experience on AirBnB than at a hotel, but also much more likely to have an unexpectedly awful one.

This makes reviews from other travelers absolutely key to making smart decisions about where to stay. AirBnB is also nice enough to build in translation tools so you can more or less understand reviews written in other languages than your own, something that makes a big difference in places that are further off the beaten track and where there might not be a critical mass of English-language reviews.

My general policy with reviews is to completely disregard any place with less than three reviews and any place rated below four stars. Four star places and ones with less than seven or eight reviews get increased scrutiny&mdash;oftentimes, a place that has a lower cumulative rating is simply suffering from one deeply unhappy guest who may have had a problem that you find irrelevant or absurd, so they're worth a shot. 

I also read pretty much all of the reviews available.[^2] This enables me to catch things that might not be a red flag for others but could be for us, like flaky internet or a lack of kitchenware.[^3]

#### Red flags

While we've generally enjoyed all of our AirBnB experiences, there have been a few places we've stayed that we wouldn't go back to. In hindsight, there were some things we should have realized were bad signs, so here are a few simple things to avoid.

**Way cheaper than average**

This one should be a no-brainer, but if a place is significantly cheaper than similar properties and yet you can't find anything wrong with it from the listing, chances are that doesn't mean there's not something wrong with it. 

We've made this mistake once and narrowly avoided it another time: We stayed in one place that, though it looked good in pictures and wasn't too bad on the inside, was basically a converted storage unit in a totally dilapidated building. We had to climb a set of bent and cracked wooden stairs and go out onto what looked like a fire escape just to get in and out. It also had a kitchen sink that was positioned in such a way that you couldn't wash anything in it without leaning way over into the corner, leading to constant back pain.

On the other hand, we nearly booked another place that had dozens of positive reviews, until we came across just one that pointed out that the place was basically an unsafe death trap in the half-basement of an old Soviet building with leaking gas pipes.

Now, we just dismiss outright places that don't pass the sniff test of a reasonable price, even if it means we might pay a little bit more.

**Hard-to-fix problems**

There are some issues with an apartment that are trivial to fix: we saw one place in Moscow get raked over the coals by review after review for its lack of a shower curtain, and we thought "good grief, host, just buy a shower curtain and you can probably raise the rent by enough per day to pay for it in one stay." Heck, if we were staying in the place for long enough, we'd probably just buy one ourselves.

Others, though, are much more complicated for a host to fix, so if you see people complaining about them, you probably want to think about how much of a problem it is going to be for you if you encounter it.

We've stayed in flats that have toilets that emit deeply unpleasant smells, and it's a really tough problem to fix for the owner of a single apartment in a massive building with all-up problematic plumbing. We've also run into several showers that struggle to maintain a comfortable temperature, inevitably ending up either cold or scalding hot.

We've also seen several places that report insect problems, and again, in many cases the owner's ability to deal with the situation is constrained by the bad behavior of their neighbors. When you share walls with people who don't keep their own flat clean, all the cleanliness in your own apartment won't prevent you from some cockroaches coming over to visit.

Basically, for any given complaint you see in the reviews, it's worth thinking about whether the issue is actually real (because it's entirely possible the guest is just a special snowflake), whether it would bother you, and whether there's any good way for a host to take action to fix it, even if they say they did.

### Now that you've picked a place

You've found a lovely little apartment in just the right part of town. Now what?

#### Write your host a nice note

![The note really does matter, so leave a good one][picAirBnBNote]

Unlike a hotel which might have a dozen employees who might not even all know one another, with an AirBnB you're usually dealing with a single person or a family, and so it's really important to treat them like the human beings that they are. When you go to book a room, AirBnB prompts you to write a little something about yourself. I always write a brief message to the host, aiming to convey three key points:

- We're actual human beings, too
- We're long-term travelers, so we are used to all sorts of situations[^4]
- We aren't going to destroy their home

This note sets up all of your future interactions with the host to be positive, so I tend to spend 5-10 minutes on it.

#### Research how to get there

Unlike hotels that usually have signage to make them relatively easy to recognize from outside, an AirBnB can be tricky to locate. The landlord will usually provide helpful information, but it may or may not be enough to enable you to navigate to where you're going.

We always try to find the location pre-emptively in Google Maps and pin it, but we've been thrown for a loop by the supposed location being a fair ways off. In particular, the location that the AirBnB app points you to has, in our experience, been less reliable than actually searching for the address manually in Google Maps. 

Finally, always read through the reviews, because people will often call out some useful tips on finding the place (or other useful things that might be located nearby).

#### Verify any legal requirements

Your host may or may not know or give you the full rundown of your legal obligations when traveling, particularly in another country, and at the end of the day it's your responsibility to know what's required for your stay in a place to be above board. 

One specific thing to keep in mind and research is whether you have to get registered as a foreigner in that country. In most countries, hotels will take care of these registration requirements automatically, but an AirBnB isn't necessarily able to make this happen for you without your involvement, which can lead to unexpected detours or expenses. We were surprised when the first thing we had to do when we arrived in Macedonia was walk with our host to the police station (and then go back on our own to "sign out" on our last day), and we had to pay about $20 to get registered by mail in Russia. 

The good news is that this information isn't difficult to find or resolve. We generally search for something like "(country) tourist registration" and a few minutes later we have our answer.

### Ready to book?

Awesome. Have a wonderful trip and let me know how it went! And once you do stay, make sure you leave a review that can help the next set of guests make their decisions.

And finally, if you found this guide useful and you're just signing up for AirBnB, you can [throw some Free Money my way][1] by using my referral link to sign up: you get $40 and I get $20 to use toward our travels. 

On the other hand, if this guide steered you away from using AirBnB, you can [find a delightful hotel on Booking.com][2] through this link and get $15 instead.

[^1]: Note that, particularly for guest houses that sometimes post rooms on AirBnB, it's actually worth checking if you can find the same room elsewhere, like on Booking.com, because it'll usually be cheaper. In exchange, though, you might have to pay cash for your room on arrival, rather than having all of the money automagically handled through AirBnB.

[^2]: I've given up when places have had really vast numbers of reviews, but I'm willing to go through the first 50 or so.

[^3]: One thing to keep in mind is many hosts run multiple properties, and may have reviews for other properties that make up for a lack of reviews on the one I'm actually looking for. In general, a good host won't rent you a bad apartment.

[^4]: This one seems especially pertinent for Americans who have a well-deserved reputation for having more substantial demands for a place to meet their American comfort levels.

[1]: https://www.airbnb.com/c/arcadiyk1 "Sign up for AirBnB"
[2]: https://www.booking.com/s/6ce1df39 "Book your room through Booking.com"

[picAirBnBHead]: <%= root_url %>/2017/04/airbnb-in-europe/airbnbscreen_head.png 
[picAirBnBMap]: <%= root_url %>/2017/04/airbnb-in-europe/airbnbscreen_bigcitymap.png
[picAirBnBRoomType]: <%= root_url %>/2017/04/airbnb-in-europe/airbnbscreen_roomtype.png
[picAirBnBAmenities]: <%= root_url %>/2017/04/airbnb-in-europe/airbnbscreen_amenities.png 
[picAirBnBNote]: <%= root_url %>/2017/04/airbnb-in-europe/airbnbscreen_note.png
