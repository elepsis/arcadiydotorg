---
title: Fighting the contrarian urge
date: 2012-03-10
category: Technology
tags: learn, programming
---

A few weeks ago I was chatting with an acquaintance who wanted to learn to code. He was stuck in a bit of analysis paralysis, trying to decide on the "best" programming language, IDE, framework, etc. to learn was. I've been there too: at one point in my life I literally flipped a coin on whether I was going to go learn Ruby or Python.

My advice to him was simple: just pick something and run with it. As Teddy Roosevelt [supposedly][1] said, "In any moment of decision the best thing you can do is the right thing, the next best thing is the wrong thing, and the worst thing you can do is nothing." So we talked about what he wanted to do (build a game), what he had already considered (Java, because that's what Minecraft is written in), and what criteria he was using to pick something (not terribly well thought out ones) with a goal of landing on a decision.

As we had this discussion, I thought back to how I've made these decisions myself, and I realized that once I was done rationally narrowing my options to ones with a similar set of pros and cons, I had a worrisome tendency: to choose *less popular* options, even if they made my life harder.

### How my contrarianism let me down
To give you an example, last year I attempted to go through [Michael Hartl's excellent Rails Tutorial][2]. This is a fantastic free way to learn Rails: it aligned with my goal (learn how to build a web app the right way, rather than cobbling together a bunch of PHP spaghetti), and it would walk me through all the steps required to build and deploy just such an app. But it's also an *opinionated* tutorial, in that it recommends you use specific tools as you go along: Git for source control, [Heroku][3] for deployments, [Blueprint CSS][4] for layout (though the latest version of the tutorial has now switched to [Bootstrap][5]).

Being the contrarian that I am, I wasn't having *any* of that. I decided that I was going to use Mercurial for source control, YUI CSS to help layout content, and deploy to my own VPS. This would have been just fine if I could competently use all of those tools, but I really couldn't. The net result was that I expanded the amount of stuff I had to learn exponentially: not only was I learning Rails and Ruby, but now I had to add to that working with a CSS framework, managing my own Linux server and jumping to distributed source control for the first time. Whereas the tutorial gave me the bits of information about its chosen tools I would need to know, I now needed to intentionally ignore them and figure out the corresponding way to accomplish things in my chosen tools.

You might have inferred the results of these decisions from my word choice. Having to spend so much time working on incidental things slowed my progress through the tutorial dramatically, things got busier at work, and I ultimately got distracted. I gave up on the tutorial around chapter 7. And this isn't the first time I've had a similar experience.

### Geeks are contrarian by nature
I think I'm not alone in making these mistakes, and I think they're especially prevalent among people in software for one major reason: Engineers and geeks have a strong tendency to be contrary. Many people end up in these areas precisely because of an unwillingness to accept the status quo in some area, and to fix something most of the world doesn't perceive as broken.

The [recent][6] [debate][7] [on Hacker News][8] about whether it is a good thing or not that lots of sites are adopting Bootstrap is just one example of this. Just as this tool becomes popular, the programming community begins to push back against it. I guarantee there's a non-trivial number of people who will now resist using Bootstrap, even if the attention it's gotten is going to build a strong ecosystem around it that will make it perhaps the best choice for their uses.

Sometimes this might have a great outcome: we'll get another tool that does something a little different and is perfect for a different set of scenarios and people. But that's what happens when an expert decides they don't want to use the tool because it doesn't meet their needs, not when a newbie (like me) is steered away because "it's overpopular" and "all sites built this way look alike."

As we make choices about what to invest our time and effort into, let's keep in mind that making life harder in one area is a great way to reduce the bandwidth you have to invest in other things. I'll certainly try to fight my contrarian urges; I hope you will as well.

[1]: http://en.wikiquote.org/wiki/Theodore_Roosevelt#Disputed "Teddy Roosevelt quotes"
[2]: https://www.railstutorial.org/ "Ruby on Rails Tutorial"
[3]: http://www.heroku.com "Heroku"
[4]: http://www.blueprintcss.org/ "Blueprint CSS"
[5]: http://getbootstrap.com/ "Bootstrap CSS"
[6]: http://news.ycombinator.com/item?id=3675783
[7]: http://news.ycombinator.com/item?id=3676179
[8]: http://news.ycombinator.com/item?id=3685909