---
title: Talking at work
subtitle: Want a team to communicate well? Talk to groups at least 80% of the time
date: 2015-08-04
category: Career
tags: culture, communication, work
---

The rise of [Slack][1] and [similar][2] [chat][3] [services][4] is, in many ways, changing the way teams communicate at work. Day-to-day conversations are increasingly moving from the stagnant, high-overhead world of email to light-weight chat. I find this evolution incredibly exciting, and I think many companies and teams are benefiting tremendously from these changes. But most teams? Well, they never knew how to communicate in the first place. And Slack can't magically fix the fundamental flaws that make bad teams remain bad teams.

![Poor communication][picCommunication]

### How do you know a team is bad at communicating?

My experience is that if you've ever been on a team that has healthy communications and you suddenly find yourself on a bad one, it'll be painfully and overwhelmingly apparent. But I suspect quite a few people haven't had that good fortune, and believe a team with stunted communication channels is actually the norm.

Here are some pretty safe indicators that you're on a team that doesn't know how to communicate:

READMORE

- **Your life is surprises and fire drills**: Because your team isn't talking well, you don't know about work that's coming your way until it actually arrives. Suddenly, you have to drop everything and deal with the issue of the moment&mdash;until the next day, when you have a new issue of the moment. And you have an at best minimum idea of what's driving the priority of any of these fire drills.
- **Making things happen requires people, not teams**: Have a question about some aspect of your product? Need something related to said product to change? Well, you could start with the team that is ostensibly responsible for that aspect of your product, but you know that might not be effective. Rather, your best bet is to find the last people who worked on it, and someone in leadership who is influential enough to make sure that whatever change you're proposing happens. This pattern repeats itself constantly, and over time people build up a mental map of the "actual deciders" which is only tangentially related to your team's actual org chart.
- **Getting up to speed takes months**: If you don't have the aforementioned mental map of decision makers built up, you find that getting things done is an exponentially longer process. Better still, even if you are now ostensibly responsible for some aspect of your product, you're going to be bypassed in favor of reaching out directly to the former experts, who will hopefully be gracious enough to include you in any responses. Five to six months later, you'll finally start worming your way into people's mental maps&mdash;just in time for your responsibilities to shift.
- **Nothing is written down**: Your team is executing on plans that no one has ever seen, based on decisions that were made by an ad-hoc group in hallway conversations and then not shared. Progress is tracked according to a set of metrics that are opaque to everyone, and no one has any idea of when the work will be done because creating a plausible timeline is impossible in these conditions.

The end result is that most communication happens through ad-hoc hallway gossip. The leadership is disconnected from the people actually doing the work, who have no understanding of the priorities and pressures that are driving decision making. Information becomes power, and the biggest blowhard talkers become the brokers of that power. Even if that team is somehow healthy in the short term, it's just a matter of time before it wrecks itself.

So, if you're caught in this situation, how do you go about fixing it?

### The 80% rule

The common theme running through the above problems is **individual-oriented communication**. People believe that the only way to get things done is based on who you know, not on the structures that are in place. Consistently opting for this kind of communication is an explicit statement of mistrust in the division of teams and responsibilities your organization has come up with, and we know what happens in situations where trust doesn't follow structures: [the structures evolve into a basket case][5].

Now, it's possible that your teams are indeed organized poorly, and that's a wholly separate problem from this article. But it's equally likely that your team structure is fine, and you and your team are just used to working together like you're a much smaller group where proper communication wasn't as important. If that's the case, it's up to you to make things better and shift away from individual-oriented communication.

The easiest way to think about this is to apply a very basic heuristic: **80% or more of your communication should go to a group, not to individuals**. Depending on whether you're dealing with email, messaging or some other communication mechanism, the form of this heuristic may change, but the rule still holds. For example, if you're sending emails, 80% of your emails should go to your team's mailing list, not (just) individual members of that team. And if you're using messaging, you should communicate in public channels in Slack and not in individual or small group IMs.

But we don't have mailing lists for the whole team, you say? Well, okay. Set them up. You don't have a messaging service that can accommodate your whole team or organization? No problem; [Slack is free][6]!

Following this rule addresses all the problems I called out above: surprises and fire drills are minimized because everyone sees most communication; because questions reach teams, not people, everyone can learn and ramp up faster; team ownership becomes clearer; and finally, the need for ad-hoc hallway conversations as the primary means of conveying information is minimized.

Individual communication has its place, of course: if you need something specific from a particular individual (e.g. "Can you send me a link to that thing we talked about?") or, more seriously, need to discuss topics pertinent to career, compensation or other information specific to an employee, that may by all means be the right option. But the best thing you can do to improve communication on your team overnight is to ask yourself three questions every single time you communicate:

1. Does what I'm writing really need to be private? Why?
2. What's the *broadest* possible audience I think this is relevant to?
3. Are there any teams outside the primary audience I have in mind that I should make sure to include?

### The false downsides of overcommunicating

Some downsides people come up with for sharing broadly are entirely legitimate, but many of them are nothing but excuses to keep doing things the way people are used to. Here are a few bad reasons to not err on the side of communicating broadly:

- **I don't want to spam everyone**: Look, your colleagues are grownups who know how to manage their inbox when it's filled with *actual spammers*. Trust them to read what they care about and ignore or filter out what they don't. But don't try to predict ahead of time what will and won't be relevant to them if you can help it.
- **I'm just not ready to talk about this with a broad group**: If you set proper expectations with your team that you'll share things at a very early point, you'll get fantastic feedback far earlier than you would otherwise, and make better decisions.
- **I'm just trying to protect my team from managerial craziness**: Sometimes knowing that your management is crazy is an incredibly valuable bit of information. Much as it's critical for managers to serve as a [shit umbrella][7], it's even more critical for them to pass through the information people need to do their job well. And to reiterate, you probably can't predict the relevance of much of that information ahead of time.

Look: if billion-dollar payments startup Stripe can maintain a [radical transparent email policy][8], does your organization really have any excuse to not start communicating at least a little more broadly?

### Why does it matter?

When an organization suffers from poor communication, its effectiveness is reduced dramatically. Taking months to have new employees ramp up isn't free. Having to re-fight all the fights because no one wrote down and/or shared the outcome last time isn't free. And of course, fire drills are worse than not free: they annihilate your team's ability to do long-term thinking and be proactive about the issues that are going to be critical in the long run.

Most importantly, **a team that doesn't communicate well is a bad team**. It doesn't matter if your work-life balance is great, if the product you're working on is successful in the marketplace, or if everyone is being paid buckets full of straight cash: all of these things are transient if the team's communications problems aren't aggressively and rapidly addressed.

If you find yourself on a bad team like this, you can be the first to start the ball rolling in the right direction: start following the 80% rule, and encouraging your coworkers to do the same. The best people on your team will thank you.

*Photo credit: ["On the Can" by Fujoshi on Flickr][9].*

[1]: http://www.slack.com "Slack"
[2]: http://www.hipchat.com "Hipchat"
[3]: https://campfirenow.com/ "Campfire"
[4]: http://gitter.im "Gitter"
[5]: http://www.slate.com/articles/business/small_business/2012/07/the_small_business_problem_why_greece_italy_and_spain_have_too_many_small_firms_.html "Southern Europe's Small Business Problem"
[6]: https://slack.com/pricing "Slack pricing"
[7]: http://bitquabit.com/post/coding-is-priority-number-five/ "Coding is priority number five"
[8]: https://stripe.com/blog/scaling-email-transparency "Scaling email transparency"
[9]: https://www.flickr.com/photos/fujoshi/12181669905/ "On the Can"


[picCommunication]: <%= root_url %>/2015/08/talking-at-work/communication.jpg
