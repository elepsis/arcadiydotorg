---
title: Tragedy of the common timeline
subtitle: Meerkat isn't good for Twitter's mainstream users. Why are we surprised it got blocked?
date: 2015-03-13
category: Technology
tags: product, web
---

If you keep up with the tech industry's latest social fads, you might have seen a new app blow up this week: [Meerkat][1]. The iOS-only video streaming app enables users to trivially kick off a live broadcast of whatever they may be doing, and share it with their Twitter followers.

On Friday evening, only a short while after the app began to draw hundreds of influential users, [Twitter pulled the plug on Meerkat's access][2] to their users' social graphs, preventing the app from drawing on people's existing networks of Twitter followers. Given that this comes as Twitter works to rebuild developer trust, it's a controversial move, especially in light of the announcement that [Twitter has purchased a Meerkat competitor][3]. 

![Is Meerkat doomed?][pic]

Given that Meerkat can still freely and automatically post to Twitter otherwise, it seems that Twitter has taken this stance primarily for competitive reasons, and I'm not entirely comfortable with Twitter becoming protective of the parts of my social graph that happen to be in its system today. While an outright block is probably too harsh a measure, I think Meerkat runs the risk of running afoul of Twitter and undermining its own growth for another reason entirely: **the app is a prime polluter of Twitter timelines**.

READMORE

### Great timelines make great users

A Twitter timeline is a shared resource: every tweet is competing for people's attention, and the quantity and quality of tweets in someone's timeline at any given time has to be compelling enough to get them to read (and hopefully engage with) what people are writing. 

That attention is worth real money to companies and celebrities, who often spend large sums on hiring experts to maintain their social media presence, but it's absolutely critical to Twitter itself. This is one of the reasons Twitter has struggled with onboarding and keeping new users, who may not know how to fill their timeline with things they care about.

So when a service starts using Twitter's platform and makes the content of someone's timeline less compelling, Twitter has to weigh openness against the risk of users leaving because their timeline is less interesting.

Why do I think Meerkat makes a user's timeline less compelling, though? One simple reason: **context**. 

### Waste in the stream

A similar polluter gained popularity on Twitter several years ago. TweetChat is an app that let people participate in a Twitter-based group chat loosely connected by a hashtag. You may have seen one of these chats; it's common for them to include prompts and questions that explicitly start with a numbered question: "Q12: Are you tired of seeing this crap in your timeline yet?"

TweetChat built functionality for people participating in these chats on their site that makes sense in the context of its app. Unfortunately, everyone else reading the tweets of someone participating in one of these chats only sees disjointed questions, answers and responses that don't stand well on their own. And because the chat is an active conversation, a person who might normally only tweet a few times a day can suddenly put 30 of these context-free tweets in your timeline.

Meerkat propagates the exact same issue: literally the [first rule of Meerkat][4] is "everything that happens on Meerkat happens on Twitter," so anything you say on Meerkat is tweeted out from your account. And because in the case of Meerkat the conversations revolve around a live video broadcast, it's incredibly unlikely that anyone outside of the stream has any idea of what the conversation is about. This is doubly true if you're catching up on tweets from a few hours ago and the broadcast is long over, since Meerkat doesn't allow replays.

The one saving grace of this functionality is that thanks to how Twitter handles replies, you have to be following everyone involved in a Meerkat conversation for those tweets to show up directly in your timeline. But I've seen that happen surprisingly often in my often-overlapping follower network.

### Cleaning up the mess

I suspect Meerkat the company as well as its fans would say that the conversations can be compelling and that each of these tweets encourage more people to jump onto the stream. That may be true, and of course I can always unfollow or (because I use fancy Twitter clients) mute one or more of the people using the service. But it's worth considering what these tweets look like to those who are new to Twitter and therefore might not understand how to avoid them.

While there's no hard and fast rule Twitter can impose to prevent services like Meerkat from polluting their users' timelines, what it could do is recommend a guideline: Tweets posted by an app or service on someone's behalf should not require a reader to use that app or service to make sense of them. Developers who flagrantly abuse that guideline could be restricted in various ways, like rate limiting their ability to post tweets on behalf of users. As a result, developers might think long and hard about when their app should and shouldn't tweet.

The thing about the common timeline is that the people and brands using tools like Meerkat are obviously concluding that the engagement from folks willing to use the app is more valuable than the annoyance to those who aren't interested or who are catching up after the fact. Given how much work people put into posting compelling, fun or insightful tweets most of the time, it's fascinating that they're willing to take that risk.


[pic]: <%= root_url %>/2015/03/tragedy-of-the-common-timeline/meerkat.png "Is Meerkat doomed?"
[1]: https://itunes.apple.com/us/app/meerkat-tweet-live-video/id954105918?mt=8 "Meerkat in the App Store"
[2]: http://www.buzzfeed.com/mathonan/twitter-chokes-off-meerkats-access-to-its-social-graph#.ej7AEG1Yn "Twitter blocks Meerkat's access to its social graph"
[3]: http://recode.net/2015/03/09/twitter-has-acquired-live-video-startup-periscope/ "Twitter has acquired live-video startup Periscope"
[4]: http://meerkatapp.co/rules "Rules of Meerkat"