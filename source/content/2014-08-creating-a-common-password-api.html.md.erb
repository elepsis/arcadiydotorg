---
title: Creating a common password management API
date: 2014-08-13
category: Technology
tags: api, password, pm
---

Earlier this year, [I spoke at Seattle's fantastic Ignite event][1] (the 24th edition, in my case) about this subject. This is my second post on the problems with managing passwords in the modern world of technology; you may wish to read [the first part][2], where I describe why this is a problem and why the current solutions aren't good enough.

In the meantime, here's my speech from Ignite, in case you'd like the five minute version!

<iframe src="http://www.youtube.com/embed/fvJBRBggtyQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### A common password management API

One of the worst things about passwords today is that every single site is required to reinvent at least a portion of the wheel of password management. Because there's no fundamental standard for how passwords should be handled across the internet, sites make up their own rules, often badly.

Two of the most common mistakes are:

1. **Storing passwords insecurely.** Many sites are guilty of trying to encrypt a password but not using a terribly secure method (e.g. they hash the password, but not [securely][3], or use an easily crackable encryption mechanism). Others fail even harder and just go all out to [storing your passwords in plain text][4].
2. **Adopting [incredibly painful, specific requirements for passwords that might be considered secure][5]**. All of us have undoubtedly found a site that requires such a peculiar permutation of password length (too short? Nope! Too long!), required character classes (a capital letter, AND a number, AND a special character, and maybe some other things) and miscellany that you're more tempted to give up than to actually finish registering an account.

Unfortunately, there's no silver bullet for forcing a site to implement a sensible password policy. But what we can try to solve is the pain a user has to feel, by standardizing as much of the process as possible.

The way we do that is by **defining a standard API that can abstract the pains of password management away from a user**. With a common password management API, users can rely on a password manager to create unique, secure passwords for each site, minimizing the impact of a plain text password being stolen, and apps can be created to handle obnoxious password policies automatically. Here's how.

### Making a common API work

Different services aren't going to stop specifying their own complicated password requirements overnight, so any common API has to handle an assortment of those requirements. As such, a site must be able to describe its requirements, and a client must create passwords that comply with them.

There are lots of models in computing that follow this model today: many protocols, for instance, have a notion of [allowing the server to specify its supported methods][6] to a client before that client issues any further calls.

In the case of this common API, you could imagine an app making a call for supported formats:
	
	GET http://site/API/PasswordReqs

And then receiving a response that described the accepted passwords:
	
	{
		"min_length": 8,
		"max_length": 16,
		"supported_chars": "aA#! ",
		"required_chars": "aA#"
	}

Once a client retrieves the information on the passwords that are supported, they can proceed to setting a new password.

In no case should a client be able to read an existing password&#151;after all, a site shouldn't know what it is anyway, because they're encrypting it from the get go.

### Making a common API secure

One of the advantages of standardizing the API but not the implementation is that services don't have to worry about trusting a third party to not compromise their users (like they might if they relied on, say, Facebook IDs).

A standard API does come with its own security issues, however:

1. You allow potentially untrusted apps to try to interact with your passwords, creating assorted impersonation issues.
2. If there's a fundamental design flaw in the API, many sites could be vulnerable.

I consider the second issue to be of limited importance; any API that would win adoption from a reasonable proportion of the top sites on the internet would be reviewed by lots of smart people who should largely catch any fundamental flaws. The first issue, on the other hand, is more interesting.

Thankfully, we already have a model for how to mitigate that concern: today, when apps request permission to access something like a Facebook account, [they can already ask for a specific set of rights][7]. Extending this to password management, then, is as simple as creating a new permission scope and making sure users are explicitly opting in to it for their apps. Ideally, most users will only grant password management rights to the one or two apps they've chosen to manage their passwords across different devices.

### The end goal

Ultimately, if a significant number of sites were to adopt this mechanism for managing passwords, you could add the "killer feature" to a password manager: a change all passwords button that would go through and automatically update your passwords on all the sites you use. Indeed, once such a button exists, it may even make sense to have the app automatically change all your passwords everywhere, say every 90 days.

With this functionality, password managers could truly manage your passwords end-to-end, and the number of times where you as a user have to interact with the original site's password management would be reduced to registration or errors only.

### Adoption is the challenge

If there's a fundamental flaw to this proposal, it's that it relies on a bunch of self-interested actors (assorted web companies) to do a significant amount of work that won't directly earn them any money or users. I don't have any easy solutions for this.

However, companies have been pushed to standardize other facets of their services in the past: for example, by convention most email services use the same general mechanism for reporting abuse: an email to abuse@service.com will almost always be addressed. In general, these conventions spring up when there's universal value to all services from making the standard available, and there is indeed value to services from having a common password API:

1. It makes it more likely that users will use password managers, thereby ensuring that **fewer users will use one or two insecure passwords all across the internet**, and
2. It **makes it far easier for a user to fix the situation in the event your site is hacked**, reducing a user's temptation to give up and close their account. Any site can find themselves unexpectedly vulnerable to a security issue, and reducing the stress associated with this scenario for a user is a huge win in and of itself.

Ultimately, I suspect that it'll only take a few more Heartbleed-class bugs before an initiative like this would see some traction.

[1]: http://igniteseattle.com/2014/07/17/arcadiy-kantor-change-all-the-passwords-without-pain-misery-and-suffering/ "Arcadiy speaks at Ignite"
[2]: http://arcadiy.org/2014/05/common-password-management-api-1/ "We need a common password management API"
[3]: http://codahale.com/how-to-safely-store-a-password/ "How to safely store a password"
[4]: http://plaintextoffenders.com/ "Plain Text offenders"
[5]: http://research.microsoft.com/apps/pubs/?id=132623 "Where Do Security Policies Come From?"
[6]: http://www.wftpserver.com/kb_basic.htm "FTP Basic Knowledge"
[7]: https://developers.facebook.com/docs/facebook-login/permissions/v2.1 "Permissions with Facebook Login"