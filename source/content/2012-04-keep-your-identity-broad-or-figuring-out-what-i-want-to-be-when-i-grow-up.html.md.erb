---
title: Keep your identity broad
subtitle: Or, figuring out what I want to be when I grow up
date: 2012-04-02
category: Career
tags: identity, influence, philosophy
---

In Jostein Gaarder's remarkable novel "about the history of philosophy," [Sophie's World][1], the plot kicks off with the title character receiving an anonymous postcard in the mail, asking a seemingly simple question: "Who are you?"

The simplicity is, of course, a deception. It often seems the more important something is, the more difficult it is to articulate, not to mention fully understand and apply. This is true of everything from scientific concepts, where the theories that come closest to explaining our universe are often some of the hardest to grasp, to the techniques and skills that, in art or sport, push a performer from just good (and therefore nothing special) to great (and famous, and popular, and so on). And it's also true of the things that apply to each person individually: figuring out what makes you happy is critical to, well, happiness, but for most of us it is an elusive, lifelong quest. Understanding personal happiness, though, is just part of answering that question: *Who are you?*

I haven't gotten any mysterious postcards lately, but the question has been on my mind as I try to move from a perception of myself I've recently realized is rather amorphous to something more crisp and definite.

### The power of identity
A person's understanding of who they are guides a remarkable percentage of their day-to-day behavior. Robert Cialdini, one of the most notable researchers on influence, [notes that identity is an incredibly strong lever][2] in triggering an activity: for example, if you believe yourself to be a person who cares about the environment, you're more likely to donate to forest conservation. This may seem self-evident at first glance, but it turns out that *if someone asks you to reaffirm that you care first*, you'll donate more money than if you're asked without that initial appeal to your identity.

This is but one (paraphrased) example. Your identity also determines the things you like and dislike, and the amplitude of your reaction to discussions about a topic are strongly determined by how close you hold it to your identity. Paul Graham notes that most online discussions of religion and politics inevitably turn into flame wars because these things are so core to most people's identities, and recommends you [keep your identity small][3] to avoid getting into petty fights about things that may not ultimately be important.

### Keep your identity broad
The problem with keeping your identity small, though, is that a "broad" identity is one of the things that makes a person *interesting*. Graham writes that "the more labels you have for yourself, the dumber they make you," but oftentimes those very labels allow you to bond with people, make impassioned (and well-reasoned) arguments, and accomplish improbable things.

Some of the most fascinating people I know are those who have integrated a broad and varied set of beliefs or activities into their identity. These people achieve the most remarkable things. And more often than not, thanks to their broad identities, they're the ones with the largest, most diverse networks of friends and compatriots.

A broad identity doesn't&#151;or at least shouldn't&#151;mean you cannot have reasonable discussions about what you believe as long as you're mindful of why you have those beliefs and are willing to re-examine the assumptions that led up to them. All it means is that you have a diverse range of interests. Subjectively, I suspect it also means you are happier, because you have more groups you feel you belong to.

### Who am I?
While I've never been shy about my opinions on a wide range of topics, I have been extremely hesitant in letting things become a part of my identity.

I know from past experience that when I identify with something, it is a long-term commitment: once I joined [AIESEC][4] in college, I dedicated several years of my life to it, traveled to a number of countries, and attended over a dozen conferences. I remain involved as an alumnus to this day. I spent more than four years writing for [my college's newspaper][5]. But those activities set a high bar: I am hesitant to do things I'm not sure I can fully become invested in.

Some parts of my identity, like the extent of my nerdom, are also things I keep private most of the time. I've probably seen more Japanese television, both live action and animated, than most people attending anime conventions, but I no longer feel like this is a large enough part of my identity to attend myself.

The net result of all this is that while I find a lot of things interesting, I haven't been inclined to commit to some small set of them. And the outcome, predictably, has been that sometimes I bore even myself. I feel increasingly that my unwillingness to expand my identity has held me back from experiences that could have a material effect on my happiness.

Even if I risk being manipulated by cunning influencers or getting into online flame wars, I will expand my identity, though I don't yet know in what direction. I think the benefits will be worth it.

[1]: http://www.amazon.com/gp/product/0425152251/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=0425152251&linkCode=as2&tag=arcadiyorg-20&linkId=S76QF2QYZYLZTJIB "Buy it on Amazon"
[2]: http://www.amazon.com/gp/product/006124189X/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=006124189X&linkCode=as2&tag=arcadiyorg-20&linkId=TOMCWLDS4GY63GLC "Buy the book on Amazon"
[3]: http://www.paulgraham.com/identity.html "Paul Graham - Keep your identity small"
[4]: http://www.aiesec.org "AIESEC"
[5]: http://nique.net "Technique"