---
title: Tourism, good and bad
subtitle: Can tourists visit beautiful places without destroying what makes places worth visiting?
date: 2017-09-21
category: Travel
tags: reflection, tourism, economy
---

During the first leg of our round-the-world adventure, we spent a few days in Dubrovnik, one of the most beautiful cities in the world. It's best-known for its massive, incredibly well-preserved city walls and beautiful old town, and of course for being on the clear, blue Adriatic sea, and as a result it's been used as the backdrop for everything from the Game of Thrones series to the newest Star Wars films. But really, here, judge for yourself:

![Dubrovnik, Croatia][picDubrovnik1]

![Dubrovnik, Croatia][picDubrovnik2]

From the perspective of sheer natural beauty, Dubrovnik is one of the loveliest places I've ever been. And yet I couldn't help but feel like there was something missing. With its lovely harbor and easy proximity to other destinations in and around the Mediterranean, three to four cruise ships worth of people pour into the small city center every day. That's to say nothing of the huge numbers of people who arrive straight to Dubrovnik on one of many low-cost flights from all over Europe or bus in from neighboring countries.

Last year, more than a million people [were expected to visit Dubrovnik's main attraction][1]: the city walls. And the old town, being quite compact in size (the aforementioned surrounding city walls are only about 2 kilometers in diameter), has a frankly overwhelming concentration of tourists. The result is that Dubrovnik feels in many ways like one of the cruise ship destinations in the Caribbean, but with more history and more in-town activities besides hanging out on the beach. 

That makes the city appear like a city of and for the tourists, rather than a living, breathing, actual place that has its own daily happenings that go on irrespective of whether anyone comes to visit.

READMORE

### Okay, but what's wrong with being a tourist city?

Tourism can provide jobs, generate necessary funds for a city to maintain its historic sights, and boost a city's reputation, so it's certainly not all bad. But at the same time, [unchecked tourism can actually push real life out of a city][2], as businesses that address the mundane needs of locals (whether shops for household goods, clothing and food ingredients or businesses like hair salons and locksmiths) are replaced with yet another souvenir store or generic restaurant. If you have to travel well outside the city to deal with the essentials of daily life, you aren't going to want to live within the city--and that's to say nothing of having to deal with the daily crush of tourists clogging your streets.

Let's look at an example that's closer to home: Seattleites generally avoid Pike Place Market on the weekends with good reason; it's so overrun with folks from out-of-town that you can't really enjoy wandering through and looking at the shops. The crush of tourism is simultaneously what keeps many of those shops in business and what keeps most locals from visiting. And while the market itself has done a great job of preserving its uniqueness, much of the surrounding area consists of chain restaurants and chain stores, which, again, aren't really a draw for locals.

The truth is that even tourists don't want to be around nothing but other tourists. There's a certain magic to observing people going about their daily lives, and it's that magic that gives cities their je ne sais quoi. What is Paris without its denizens sitting and sipping their café noirs [at the city's sidewalk cafes][3]? 

A place that reaches a certain saturation point of crowds has to improve its infrastructure to handle the influx of bodies, or people will be entirely unable to make their way around. Many heavily-touristed areas have already become pedestrian-only zones, blocking out not only cars but also bicycles, which makes locales that don't have a well-established public transit system even more challenging for locals to navigate.

Once a city loses its locals, it also loses its ability to evolve and improve, to figure out how to blend modernity with its history and natural beauty. The end state of this is a city like Venice: one that is beautiful and well-preserved, but, with its almost exclusive focus on history and tourism, one that feels past its prime in just about every way. Much as I loved visiting, the thought of "I'd be happy to live here" never once crossed my mind, nor does it seem to occur to most Italians. And that is heartbreaking. 

### Our role as travelers

It would be incredibly hypocritical for me to say "stop traveling to these places" after having traveled to so many of these places myself. But there's a different--perhaps better--way to visit these places, one that can perhaps tilt the balance towards the good tourism can do. It's what we've aspired to during our travels, and though we haven't always been successful, we hope we were less a part of the problem than most.

#### Stay longer

The most extreme tourist impact seems to happen in the places where it's common for tourists to pop in for just a day or two, rush through an extreme point-to-point-to-point itinerary, and get out without having really experienced it at all. Cruise ship ports are the most impacted by this, in small part because some Cruise People are awful travelers, but mostly because when your entire town is structured around people only coming in for four to eight hours at a time it breeds absurd behaviors.

Tourist-oriented businesses ruthlessly target every passer-by in an effort to maximize their income in just the few hours available, and then proceed to close early. Meanwhile, places just a bit off the beaten path struggle to sustain themselves because the time-limited folks can't sacrifice their precious few minutes. And locals can forget about going out for lunch (or any other errand) anywhere in the vicinity of the tourist districts.

Cruise travelers might be the most extreme case, but folks who are only staying in a city for one or two nights aren't able to do much more, especially in larger cities with more places to see: in either case you end up with a highly-structured itinerary jampacked with only the biggest highlights. Locals hate the "just go to the top sights" culture too: there were signs all over Lisbon complaining about it.

![Lisbon does not love top 10 lists][picLisboaTourists]

If you can stay in a place for a longer time, though, an entirely different set of options open themselves up to you. You can visit the sights at off-peak hours. You can get further afield to the places that aren't part of the city's core tourist itinerary. You can make your way to places locals go to and give your money to them rather than to one of the tourist-traps. We've realized during this trip that we have much fonder memories of the places we actually got to spend time in. We now try to spend no fewer than four nights in any given place, and even when we're again limited by vacation days we're going to try to stick to that minimum. The tradeoff, of course, is that you don't get to visit as many places. But we've realized that we'd much rather truly enjoy the places we're in than squeeze in a few extra destinations.

#### Get more local

If you do embrace the opportunity to stay in a place for a longer time, you can also take the chance to get beyond the TripAdvisor top ten and open yourself up to serendipity. 

One of our favorite things to do in larger cities is to find a neighborhood that is easily transit-accessible from the main cities but rarely visited by tourists, and make that our homebase. Then we can become regulars at the local grocery or convenience store, wander into cute local restaurants that don't see much tourist business, and generally live life more as the locals do.

It doesn't mean you have to give up on doing any of the things you were going to do otherwise, but it does mean broadening your horizons a bit in terms of what you want to see and do. It does also mean that you have to increase your comfort level with potentially-awkward situations and language barriers. But I genuinely believe the tradeoff is worth it.

In Japan we spent two weeks in an AirBnB directly between Osaka and Kyoto, with either city accessible in about 30 minutes on the train, and it was one of our favorite experiences yet. It wasn't so far off the beaten path that we had any difficulties with daily necessities, but it was far enough that we were the only obvious foreigners we encountered day-to-day, and we got to experience the comings and goings of daily life. The result? We felt much more at home than in other cities where we stayed in hotels inside the city center.

#### Don't be THAT tourist

Of course, if you are trying to adopt more of a local lifestyle, you'd better make sure you act like one to the best of your ability, and that means taking care of your surroundings and respecting the expectations of the people around you. One of the reasons many tourists have such a bad reputation is because they have a bad case of Don't You Know Who I Am-itis, thinking their needs are more important than anyone else's and not being considerate of those around them. We all know the stereotype of the crazy shenanigans tourists can get up to, and the collateral damage those shenanigans do to their surroundings.

Many city residents protest against services like AirBnB because they bring transient travelers into their apartment buildings, and it turns out that some of those transient travelers are assholes. They leave trash sitting around, break things in the common areas, have loud parties in their flats and generally disturb the neighbors. The truth is that this is nothing that doesn't also happen in hotels--but hotels signed up for dealing with this sort of behavior, whereas an AirBnB landlord's neighbors didn't.

It's also vital to respect local traditions and expectations. For instance, many locals, especially those in more conservative countries don't approve of the skimpy clothing and public affection that is normal to westerners, and you should be respectful of that. It's also important to understand things like local garbage policies, to make sure you're not disposing of your trash in a way that's going to get your neighbors angry at you.

The good news is that you can choose to not be that kind of traveler. Acting like a local isn't usually difficult: it's about doing twenty minutes of research, and then the rest is just normal human decency. Just because you're on vacation and can stay up until 4 a.m. doesn't mean your neighbors can, and as long as you're mindful of that you can be a good neighbor. Just because you don't live somewhere long-term doesn't mean you should litter on that place's streets. The booking platforms, too, can help by providing better methods for neighbors to communicate with landlords and report bad travelers.

When we find an adorable neighborhood we love, we always like to imagine what it would be like to live there permanently. If we can all treat the places we visit like places we'd like to move sometime very soon, wouldn't we behave rather differently? And if we stay in those places for longer, make an effort to live life more like a local, and avoid living down to the bad tourist stereotypes, can't we change the narrative and the results of our travel? I'd like to think we can.

[1]: http://www.croatiaweek.com/dubrovnik-walls-daily-visitor-record-smashed/ "Dubrovnik walls daily visitor record smashed"
[2]: https://www.theguardian.com/commentisfree/2014/sep/02/mass-tourism-kill-city-barcelona "Mass tourism can kill a city - just ask Barcelona's residents"
[3]: http://www.messynessychic.com/2015/11/16/13-paris-cafes-an-ode-to-the-sidewalk-culture-ill-never-give-up/ "Paris cafes: An ode to the sidewalk culture I'll never give up"

[picDubrovnik1]: <%= root_url %>/2017/09/tourism-good-and-bad/dubrovnik1.jpg
[picDubrovnik2]: <%= root_url %>/2017/09/tourism-good-and-bad/dubrovnik2.jpg
[picLisboaTourists]: <%= root_url %>/2017/09/tourism-good-and-bad/lisbon.jpg