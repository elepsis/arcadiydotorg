---
title: The short list
subtitle: What does one bring on a long round-the-world trip?
date: 2016-08-24
category: Travel
tags: travel, packing
---

Here's how I normally buy clothes:

1. Find a few things I like and buy them in bulk, cheap or at least on a solid sale on the internet, or
2. Go to Ross and embrace the treasure hunt.

Needless to say, this does not produce fashionable results, but more importantly, it means that most of my clothes aren't the most durable or high quality: crazy fashion trends aside, I do think you get what you pay for up to a certain point. So when it came to packing for our [year-long round-the-world trip][1], I knew basically none of my current wardrobe was going to make it into my bag. And that's to say nothing of all the non-clothing items I needed.

To add to the challenge, Gina and I both committed to traveling without checked luggage, meaning we get one big bag and one personal item for all the things we're going to need all year long. Neither of us has any room for bringing much in the way of "optional" items, so we had to cut really really hard.

Now that we've started traveling, I wanted to show you what's in these two very full bags and how I decided what made the cut:

![The bags fully packed][picFullyPacked]

READMORE

### Figuring out what to bring

When you're starting pretty much from zero, you aren't going to be able to show up at REI and blindly walk out with everything you need. In fact, jumping straight into shopping is a great way to spend a ton of money on clothes and other junk that might be perfectly nice but won't necessarily be the things that work for a year of travel. Instead, you have to go through a process:

#### Step 1: Do some research

It turns out there are countless long-term travelers who blog about their adventures and describe their hardcore packing lists. These are a fabulous resource to start figuring out how to minimize your list, what types of clothing are worth looking into, and what items you might not have thought you needed but totally do.

As we did our research, some of the more useful lists we found were:

- If you want to know how to pack light from first principles, this is [the best advice I've found][2].
- This [non-pretentious packing list][3] was a good, more realistic-feeling take on what to pack.
- This site was more helpful for Gina, but [Her Packing List][4] is actually a useful read for both genders.
- Finally, the grand-daddy of packing light advocacy on the internet is [OneBag][5], and it remains a useful read. Reddit also has a [useful related subreddit][6].

#### Step 2: Trial runs with one-offs

Once you have a rough list of things you're going to bring, it's time to buy a few different items at a time and **travel with them**. There's no substitute for actually living with gear as you run around for a week or two and seeing whether it will really work for you.  We've taken an international trip around Thanksgiving each of the last three years, and with each one we've come closer and closer to our final packing list as we figured out what worked best.

Over the course of this process, I've replaced my backpack, gone through about five different types and brands of underwear, bought a new daybag in Japan, and (most importantly) significantly cut down the amount of clothing I'm bringing. I now have confidence in my ability to last with this packing list indefinitely, and I wouldn't be able to say that if I hadn't actually used most of my stuff for at least one trip.

#### Step 3: Pick the winners

As you figure out which items work for you, beware of *the buts*: "Oh, this shirt is so cute, but it takes forever to dry!" or "Hey, these shoes are super comfy but my feet nearly froze yesterday!"

If an item is going to be one of the fewer than 100 things that you take around the world with you, you can't afford to put up with most of these annoyances. It's worth spending a few extra dollars on a replacement that isn't as likely to drive you mad over the long run.

Not every "but" is a deal breaker, of course, and with many items you can't avoid some compromise. But it's super important to have a core set of items that are great rather than just okay for your purposes.

### Principles for packing

I came out of the above process with two results: a list of things to bring with me and a set of principles for how I picked&mdash;and packed&mdash;those things. The "what" is more personal, but I genuinely believe this set of principles applies to everyone who's trying to pack light for a serious trip.

#### Principle 0: YAGNI

This is probably obvious, but I would be remiss if I didn't mention the most basic principle of packing for travel: You Ain't Gonna Need It! If you're looking at an item and thinking "well, I'm not sure if I'll end up using this or not," the answer is almost certainly "you're not." Just leave it out of your bag.

In practice, though, there are some things that you definitely won't need until you do, and when you do, you need them quite badly. For example, having once seen a gentleman on a night train through Italy rip off the window curtains to use them as a blanket because the heating was busted, I believe in carrying a light blanket or travel sheet. In the interest of self-reassurance, I think it's okay to let yourself pack a few small things in this category. The key with these items, though, is to limit yourself to no more than 2-3 of these "maybes."

#### Principle 1: Travel stuff isn't the same as your normal stuff

The clothes you buy at home need to be comfortable, fashionable and easy to care for: you don't want all of your stuff to require an air-dry when you have a perfectly good dryer in the house. And you likely have enough storage at home to allow yourself lots of options to choose from when you get dressed each morning, so single-purpose items/outfits are okay.

When traveling, particularly for the long-term, you have to optimize for an entirely different set of goals:

- Is your clothing easy to mix and match?
- Does it stay clean for multiple wears and air-dry quickly?
- Does it fold into a compact package for travel days?

Similarly, for non-clothing, at home you can buy things that are specialized and don't need to solve every task in a compact package. But for travel, you want more general-purpose items, or extremely compact ones. The punchline is that with the exception of something like a camera or a laptop, the things you travel with likely won't be the same things you use day-in and day-out at home.

I'm not suggesting you buy all new everything solely for travel: that's expensive and wasteful. But having some items be reserved for travel is entirely reasonable.

#### Principle 2: When it comes to clothes, layer up

The only way to travel with just a carry-on across a wide variety of climates is to embrace layering. Our goal for our trip was to bring enough layers to be able to get by in weather that gets as cold as -15 degrees Celsius (5 degrees Fahrenheit). To meet that goal, I can layer up in the following way:

- Long sleeve shirt or t-shirt
- Ultra-light hoodie
- Merino wool zip-up hoodie
- Puffy jacket
- Rain jacket

That's the absolute maximum configuration of my layers, but I can mix and match between these as needed to ensure I'm prepared for any weather. All of these items fit nicely over one another, and were explicitly chosen to be able to work together in a pinch.

#### Principle 3: Standardization helps

A subtle way many people go wrong when trying to pack lightly is by having too many different things when they could be standardized. One particularly insidious one is chargers: pretty much every piece of electronics I brought on the trip (except my laptop) charges via micro USB, including my camera, my Kindle, my phone, my battery pack, etc. But it's not unusual to have folks travel with an iPhone that charges via Lightning, a Kindle using micro USB, a camera with its own custom charger, etc. Before you know it, you've added a whole bag of extra cables and chargers.

Take shirts as another example. In theory, you might think "ooh, each of these is slightly different so I can wear one when I'm a slightly different situation" but in practice, you probably won't have the "right" shirt clean and ready to go when you need it, and you'll be cursing when you realize that some of your shirts take longer to dry and take up slightly different amounts of room than others and find yourself having to make decisions based on these raw logistics.

#### Principle 4: Minimize loose items

This one is probably a matter of taste, but I pretty much think packing cubes are the greatest travel innovation since airplanes. Not only do they make packing and unpacking your bag infinitely faster and easier at each stop along the way, but they prevent your stuff from constantly shifting around your bag and reduce the inevitable instances where a set of stuff that fit just fine yesterday magically doesn't fit nearly as well today.

Also, they can do a great job of compacting your things down in a way that doesn't rely solely on the built-in pockets and zippers of your bag. That means no sitting atop your suitcase trying to force the zipper closed!

For small items or cables, cases and cubes are even more useful. You can keep them all together and protected in an easy way, and more importantly, things don't fall out and vanish somewhere in your bag. The fewer times you have to take every single thing out of your bag to find that one bottle opener that you know is in there somewhere, the better.

Here's what my full complement of clothes looks like in their cubes:
![All my clothes in their packing cubes][picFullyCubed]

### So, what's in my bag?

With all of those principles in tow, here's what I've actually brought on my trip. Photos are captioned from left to right, row by row, and links are to Amazon when possible. If you buy anything through these links, I'll be able to afford another book for my Kindle, and you'll be helping keep me sane!

#### Bags

For my large, carry-on sized backpack, I'm using an older-generation [North Face Overhaul 40][bags1]. It's not perfect, but I like that it has a separate laptop compartment that sits against your back, and it's nice that it's expandable in a pinch should I acquire more stuff at some point.

My small additional bag for small electronics and day-in day-out walking around is a [Manhattan Passage shoulder bag I bought in Japan][bags2]. It has a super useful configuration of pockets, and is the perfect size for a long day of wandering. Just as importantly, it's not a backpack, so I can easily carry it along with my backpack. (Gina went a slightly different route and has a backpack that snaps into the front of her big pack&mdash;I'd endorse that as well.)

#### My "personal item"/daybag

These are the things that I carry inside my daybag. On non-travel days, some of these usually end up left behind in the room to lighten my load.

![My personal item contents][picPersonalItem]

- **[Midori Traveler's Notebook][pers1] (PanAm limited edition)**: I wanted a notebook (or a few) to keep track of notes and jot my thoughts down on the trip, and what's better than something literally called a traveler's notebook? Plus, the Midori notebooks allow for a vast range of customization in terms of what you put in it. Mine is currently holding blank, lined and calendar refills (all with great quality paper), as well as a miscellaneous item pocket. If the original Midoris are too rich for your blood (and I get it, they're a bit absurd for what is basically a perforated sheet of leather), there's lots of [self-made versions][pers2] ("fauxdoris") on Etsy.
- **[Kindle Paperwhite][pers3] in an [Amazon case][pers4]**: E-readers are the biggest quality of life improvement that's happened for travelers since my last long trip in 2009, and Amazon's Kindle has basically won the market in the US. If you're going to read at all on your trip, you need to own one. The Paperwhite has the advantage of a backlight that lets you read without pissing off everyone on the same bus as you.
- **A smattering of teas**: I need this for survival, so enough said.
- **[Point-it dictionary][pers5]**: My sincere hope is that I never have to use this, but in the event of an emergency where I can't explain myself, this is a tiny booklet that lets you point at pictures of items and thereby make sense to people whose language you don't otherwise speak.
- **Packable shopping bag**: Shopping bags are free pretty much nowhere outside the US and Japan, so this is an extremely helpful item to carry whenever you pop into a convenience store.
- **Envelope of extra passport photos**: Some countries require visas on arrival, and it helps to have passport photos ready to go for those.
- **[Zojirushi thermos][pers6]**: Did I mention earlier I drink a lot of tea? Anyway, these are incredible. I'll make myself tea in the morning, and it'll still be too hot to drink when I get back to my hotel in the evening. It's freaking magic, I tell you. Bonus: if you put in something cold, it'll keep that drink cold, too.
- **Camera case and camera**: I'm bringing the [Fujifilm X70][pers7] as my camera, which should be an interesting experience given it is a fixed focal length 28mm (effective) lens. I'm hoping to learn more about photography, so cutting out the ability to decide on focal length will, I hope, force me to think about everything else about my exposure.
- **Band-aids**: Sometimes I hurt myself. These will help.
- **Hand lotion**. My hands dry out like whoa on airplanes, so I like to keep a lotion in my bag.
- **Spare battery for the camera**: Everyone notes my camera runs out of batteries pretty quickly, and I've seen the proof first hand, so this will keep me photographing on long days out and about.
- **[An incredibly tiny set of dominoes I bought on Etsy][pers8]**: Because sometimes you just want to entertain yourself with some games.
- **Lens cleaning cloths/wipes**: Just a few for my electronics.
- **Passport(s)**: You need them.
- **[Tiny Brainwavz headphone amplifier/splitter][pers9]**: Will let Gina and I listen to the same audio while both using  headphones, and improve the sound quality at the same time, at a negligible increase in size over just a headphone splitter.
- **[Sun-Tripper hat][pers10]**: If you've hung out with me, you know just how much I love this hat. It folds up small, it's ventilated, the bottom of the bill is a darker color to help avoid reflections, and it's adjustable to fit any size head. I own six or seven of these.
- **Pens**: I love pens, and I personally like the [Pentel Slicci 0.4mm][pers11] best.
- **[Anker USB charger (2 port)][pers12]**: When traveling, it helps to think about bang for the buck in terms of charging. You'll be limited in the number of outlets you can access, and a two-port charger that is barely larger than most one-port chargers can be a life saver. Plus, these high-quality two-port chargers tend to provide higher amperage, thereby charging your devices way faster.
- **Fitbit charging dongle**: I guess I lied about all my electronics charging with micro USB.
- **[Micro USB cables][pers13] (two)**: I have more cords, but the idea is that most of my daily charging needs are addressed by the things in my day pack.
- **Anker battery pack and short USB cord**: Turns out battery packs are an inevitability in the modern smartphone era. It's worth buying a compact one from a reputable brand like Anker or RAVPower. The one I have is slightly older technology now but still sold at a mark-up because people *used* to love it, so if I were in the market today I'd be getting [this one][pers14].
- **Travel-sized shoe horn**: Did you know shoehorns are awesome? If you aren't travelling, [go buy this shoehorn right now][pers15], and fight you some orcs. Meanwhile, I got this sweet travel shoe horn at a thrift shop on the Washington state coast.
- **Two sets of ear buds**: One from an old Zune, the other an [actually nice SoundMagic set][pers16]. Why two? Because one blocks out noise really well, making it great for planes and buses, and the other one doesn't even try, so it's safe to use while walking around on the streets.

#### Clothes

![All of my trip clothes][picClothes]

- **[Eagle Creek compressible packing cubes][clothes1] (2 sets)**: Most of my packing cubes are this kind, and I highly recommend them. You put all your clothes into them like you might into a regular cube, and then you zip another zipper around them so they get squished down. All the air is forced out and you get a smaller, neater package. Plus, the cubes are still feather-light.
- **[Ibex Shak Hoodoo wool hoodie][clothes2]**: Merino wool is one of the best fabrics you can get for your clothes, and this hoodie is made entirely from it. It's warm, compact and comfortable, and it's going to be my mainstay throughout the fall.
- **[Arc'teryx Atom LT puffy hoodie][clothes3]**: Ultra light, breathable enough to work for lots of temperatures, yet warm enough to be your jacket in winter. Mine is pictured already packed into a travel cube.
- **[Mountain Hardwear Quasar Lite rain jacket][clothes4]**: also packed into a cube.
- **Less frequently used items**: [Speedo packable swim trunks][clothes5], spare Sun Tripper hat, [Uniqlo undershirt][clothes6], all in a packing cube.
- **Winter wear**: [Merino wool hat][clothes7], [fingerless gloves][clothes8] and a [neck gaiter][clothes9], all from Ibex, and a set of [Terramar long underwear][clothes10]. As I said, merino wool is thin, feels delightful, and is basically magic as far as clothes are concerned. And as long as you aren't buying real clothes like shirts or jackets, things made out of it are actually quite affordable!
- **A backup set of clothes**: one shirt, one socks, one underwear, to be moved to my daybag if I ever need to check my big backpack.
- **Travel pants (two pairs)**: one Merrell (I forget the model, but these [Stapleton pants][clothes11] are similar) and one [Columbia Global Adventure II][clothes12], though it seems Columbia is on to their third global adventure now so I've linked the newer pants.
- **Underwear (five pairs)**: Mostly from Uniqlo, but all made from Tencel or in their Airism line.
- **Travel shorts (two pairs)**: One [Mountain Hardwear Mesa II][clothes13] (or at least a very similar pair from a few seasons ago), the other a [Columbia Global Adventure II][clothes14] (the name was so appropriate I had to buy multiple items from this line...)
- **T-shirts (five)**: [REI Sahara t-shirts][clothes15]. If I were rich, I'd still be with her&mdash;err, I mean I'd have merino wool shirts. But I'm not, so I have these in five different colors as the next best thing.
- **[Under-Armour shorts][clothes16]**: Ultra-light shorts for lounging around. The best possible item of clothing for in-hotel breakfasts.
- **[Marmot Saxon hoodie][clothes17]**: This is an incredibly versatile layer to throw into my day bag when it seems like it might get a bit chilly towards the evening, and one that started my quest to find many more light-weight hoodies for my regular wardrobe.
- **Socks (five pairs)**: All made largely from merino wool. Mostly [REI ultra-light socks][clothes18], and one pair of SmartWool light socks. I may switch one of the REI socks for another pair of the SmartWool socks, which are a bit thicker, once we get into colder weather.
- **Long-sleeve "32 degrees" shirt from Costco**: For colder climes.
- **[Columbia Silver Ridge short-sleeve button-up][clothes19]**: Sometimes it's nice to have a very slightly nicer layer, or just to add a tiny bit of variety to your wardrobe. This is mine.

#### Shoes

![My trip shoes][picShoes]

- **[OluKai Hiapo flip-flops][shoes1]**: Flip flops tend to tear my feet up, and these seem to do it far less than any cheaper pair I've ever found. Plus, they actually look inexplicably fashionable for something I'd wear.
- **[Ecco BIOM Grip shoes][shoes2]**: You have to have a good pair of shoes for daily wandering on a trip like this, and Eccos are simultaneously comfortable, supportive and resilient enough to hold up to the abuse. They're also rugged enough for some rudimentary off-roading and look good enough to be adequate for any slightly dressier occasions I have to partake in.
- **Slippers**: I kept these from the hotel we stayed at in Hong Kong a few years ago. I love slippers and bust them out for indoors wear whenever I'm in a place for any amount of time. Still, I may get rid of these at some point during the trip.

#### Cords and accessories

![Cords and accessories I'm packing][picCords]

- **[Nomadic pencil case][acc1]**: This case has lots of nicely-sized pockets for these assorted accessories to all fit sensibly. In general, Japan does pencil cases much more nicely than the awkward monstrosities kids in the US are expected to snap into their binders.
- **Spare phone charger**: In case my Anker charger bites the dust. Eagle-eyed readers might notice it's actually an old HP TouchPad charger!
- **Tiny Samsung MP3 player**: For when I don't want to use my phone and just want to listen to some music, and its USB plug. Yes, I'm a liar about electronics charging via micro USB again.
- **[Spare SD card (Sandisk Extreme 64 MB)][acc2]**: A second SD card for the camera. I'm not especially partial to Sandisk. Just find a good sale for a well-reviewed, high-speed card.
- **[Kingston USB3 SD card reader][acc3]**: My laptop comes with an SD card reader, but I have it in use for other purposes, and Gina doesn't have an SD card reader at all, so this will help get photos from the camera to the PC. Amazon shoppers all seem to agree this is the one to get.
- **Ruler**: Because sometimes when jotting down notes you really want to draw a straight line.
- **[Set of travel adapters][acc4]**: One plug for every locale we'll be going to.
- **[RayMay travel scissors][acc5]**: They're tiny, portable, and incredibly handy. Need to cut a thread? Boom. Need to trim a tag on a piece of clothing? Done.
- **Three more pens**: Because I can't help myself. Also Pentel Slicci.
- **Permanent marker**: Already useful for writing your name on stuff in communal kitchens.
- **More cables**: 3 micro USB cords, an aux cable, and a spare lightning cable in case Gina loses hers.

#### Laptop and miscellany

![My laptop and a few miscellaneous items][picLaptopMisc]

- **Zip lock bags (some quart-sized, some gallon-sized)**: surprisingly hard to come by abroad, surprisingly useful for infinitely many purposes while traveling.
- **Handkerchief**: One of those things you wouldn't think you need but end up using all the time. Trust me on this one.
- **Stuff sack for dirty laundry**: Mine is an [ultra-light Outdoor Research baggie][misc1], but you could just as well use a grocery bag.
- **[MacBook Air 13 (Early 2014)][misc2]**. Yes, my laptop is two years old at this point. It still works great, though, so why change?
- **[Booq Taipan Spacesuit sleeve][misc3]**: A high quality, thin and light sleeve to keep my laptop safe from any damage.

#### Hygiene stuff

![Hygiene stuff][picHygiene]

- **[Travel set of Tweezerman tweezers][hy1]**: if you're buying tweezers, you should buy Tweezerman tweezers. Period.
- Floss
- Toothpaste
- Travel toothbrush
- Deodorant
- [REI micro shower kit][hy2]
- Lip balm
- Q-tips
- Razor and spare blades
- NeoSporin to go
- A few spare pill baggies in case we buy any over the counter medicine on the road
- **[Shaving cream][hy3]**: I'm trying out this fancy Pacific Shaving Company stuff and it's pretty nice so far, and while it's pricy on Amazon it's a lot cheaper if you find it in stores.
- **Shampoo and body wash, in [travel containers from Muji][hy4]**: These containers don't seem to leak so far, and the shape is super helpful. I'll probably get a few more when I get a chance and replace my giant shaving cream container with them.
- **[Concentrated travel laundry detergent][hy5]**: I'm not sold on this one, but it seems to work well enough so far.
- Hand lotion

#### Loose items

![Everything else][picLoose]

- Medicine kit, featuring Tums, Pepto-bysmol, more band-aids, Dayquil/nyquil, etc.
- Backup wallet containing (mostly) transit cards for other cities/countries
- Two pens and a pencil (yes, more pens, don't judge me)
- **[Zolt laptop charger][loose1] for my Mac**: I thought if I were going to spend $70 on an absurdly overpriced spare Apple charger, I might as well get one of these fancy new chargers that is ultra-compact and can charge a few of my other devices.
- **[Monster Portable power strip][loose2]**: This, along with the adapter kit above, enables me to plug just this into a single adapter and have four "American" outlets for all of our devices. Unfortunately the model I have has been discontinued and the price of the inferior replacement (which loses an outlet in favor of a pretty useless USB port) has shot through the roof. To make matters worse, Monster seems to be the only company making power strips rated for the full international range of voltage with a reputation to protect. I'm not sure what I'd replace mine with if it broke.
- **Linen towel from [this store on Etsy][loose3]**: I was sold on linen towels as a better, less likely to start smelling awful alternative to travel towels by [this article][loose4], and so far my experience confirms their positive traits.
- Spork, for when you buy groceries on the go and need something to eat them with
- Cork screw without a blade so it's TSA-safe
- Sleep mask
- Two TSA-friendly travel locks
- Backup glasses in case
- Silk sheet/sleeping bag liner
- **[Travel money belt][loose5]**: I keep just a tiny bit of emergency cash in mine as a last resort.
- [Travel clothesline][loose6] for drying our clothes

#### Not pictured

A few things I didn't include in the pictures: a 128 GB USB flash drive, an emergency whistle, my Fitbit and a flashlight. I also bought a paperback novel at the airport, because it's a throwback to my traditions of past long trips and because I want to leave it in a communal library as my entitlement to borrow books from all future communal libraries I encounter. Finally, I kept the fancy Tumi toiletry kit case from our flight. I'm using it in my day-bag for miscellaneous items, including a ziplock full of toilet paper, which is one of those must-haves that you learn about only at an inopportune moment.

### Isn't this a lot of stuff?

Well, yeah, it is. Just because you're traveling with only a carry-on doesn't mean you can't bring all the things you need, as long as you're smart about what stuff you buy and how you pack it.

The truth is, if Gina and I wanted to be more hardcore, we could probably cut our packs down by 10-20%. As we travel, we may end up leaving some things behind if we realize we just don't use them. But as we start our trip, this is everything I have on me. Hopefully you found it to be helpful!

[picFullyPacked]: <%= root_url %>/2016/08/packing-list/fully-packed.jpg
[picFullyCubed]: <%= root_url %>/2016/08/packing-list/clothes-fully-cubed.jpg
[picPersonalItem]: <%= root_url %>/2016/08/packing-list/carry-on-contents.jpg
[picClothes]: <%= root_url %>/2016/08/packing-list/travel-clothes.jpg
[picShoes]: <%= root_url %>/2016/08/packing-list/packed-shoes.jpg
[picCords]: <%= root_url %>/2016/08/packing-list/cords-accessories.jpg
[picLaptopMisc]: <%= root_url %>/2016/08/packing-list/laptop-and-stuff.jpg
[picHygiene]: <%= root_url %>/2016/08/packing-list/hygiene.jpg
[picLoose]: <%= root_url %>/2016/08/packing-list/loose-items.jpg

[1]: http://arcadiy.org/2016/08/going-where/ "Gina and Arcadiy's round-the-world trip"
[2]: http://snarkynomad.com/ultimate-ultralight-travel-packing-list/ "The ultimate ultra-light packing list"
[3]: http://gqtrippin.com/rtw/his-rtw-packing-list/ "His RTW packing list by GQTrippin"
[4]: http://herpackinglist.com "Her packing list"
[5]: http://onebag.com "Pack just one bag on all your travels"
[6]: http://reddit.com/r/onebag "The OneBag subreddit"

[bags1]: http://amzn.to/2bKONyj "North Face Overhaul 40"
[bags2]: http://global.rakuten.com/en/store/richard/item/mp-2200/ "Manhattan Passage sideways shoulder bag"

[pers1]: http://amzn.to/2beSo6N "Midori Travelers Notebook"
[pers2]: https://www.etsy.com/search?q=fauxdori&order=most_relevant&view_type=gallery "Fauxdori on Etsy"
[pers3]: http://amzn.to/2bzRoPM "Kindle Paperwhite"
[pers4]: http://amzn.to/2bKQaNt "Kindle Paperwhite Amazon case"
[pers5]: https://www.amazon.com/Point-Travellers-Language-Original-Dictionary/dp/3980880273/ "Point it Travelers dictionary"
[pers6]: http://amzn.to/2bfYI2u "Zojirushi travel thermos"
[pers7]: http://amzn.to/2bfNYMu "FujiFilm X70 camera"
[pers8]: https://www.etsy.com/listing/172817453/dominoes-game-bamboo-domino-set-mini?ref=shop_home_active_1 "Mini bamboo dominoes"
[pers9]: http://amzn.to/2bfQ4w9 "Brainwavz portable headphone amplifier"
[pers10]: http://amzn.to/2bfPka4 "Sunday Afternoons Sun Tripper hat"
[pers11]: http://amzn.to/2bzUeEi "Pentel Slicci 0.4mm pens (3 pack)"
[pers12]: http://amzn.to/2bd5VS0 "Anker 24W Dual USB Wall Charger PowerPort 2"
[pers13]: http://amzn.to/2bfRZk7 "Anker 5-Pack PowerLine Micro USB - Durable Charging Cable Assorted Lengths"
[pers14]: https://www.amazon.com/dp/B00UV9OXNU?psc=1 "RavPower Portable Charger 7800 mAh"
[pers15]: http://amzn.to/2bDTY64 "7.25 Inch Professional Metal Shoe Horn that kills Orcs"
[pers16]: http://amzn.to/2bDVFR4 "SoundMagic E10 noise isolating in-ear earphones"

[clothes1]: http://amzn.to/2bfSef2 "Eagle Creek Pack-it Spectre compression cube set"
[clothes2]: http://www.moosejaw.com/moosejaw/shop/product_Ibex-Men-s-Shak-Hoodoo-Hoody_10292003_10208_10000001_-1_ "Ibex Men's Shak Hoodoo merino wool hoodie"
[clothes3]: http://amzn.to/2bL5tFZ "Arcteryx Atom LT men's hoodie"
[clothes4]: http://amzn.to/2bA2fJp "Mountain Hardwear Quasar Lite men's rain jacket"
[clothes5]: http://amzn.to/2bL5EBi "Men's Speedo packable swim trunks"
[clothes6]: http://www.uniqlo.com/us/product/men-s-airism-mesh-crew-neck-t-shirt-162852.html#32~/men/airism/mesh/tops/~ "Uniqlo AirISM mens mesh crew neck shirt"
[clothes7]: http://amzn.to/2bTcrw0 "Ibex Meru hat"
[clothes8]: http://amzn.to/2bL5LN0 "Ibex Knitty fingerless gloves"
[clothes9]: http://www.moosejaw.com/moosejaw/shop/product_Ibex-Indie-Quick-Link-Neck-Warmer_10261038_10208_10000001_-1_ "Ibex Indie Quick Link neck gaiter"
[clothes10]: http://amzn.to/2bL6uhr "Terramar men's long underwear"
[clothes11]: http://amzn.to/2bklP93 "Merrell men's Stapleton pants"
[clothes12]: http://amzn.to/2bg1b8j "Columbia men's Global Adventure III pants"
[clothes13]: http://amzn.to/2beH1Pb "Mountain Hardwear Mesa II shorts"
[clothes14]: http://amzn.to/2bL8Xs5 "Columbia men's Global Adventure III pants"
[clothes15]: https://www.rei.com/product/863045/rei-sahara-t-shirt-mens "REI Sahara men's t-shirt"
[clothes16]: http://amzn.to/2bTedNB "Under Armour men's Raid shorts"
[clothes17]: http://www.paragonsports.com/shop/en/Paragon/marmot-saxon-hoody "Marmot Saxon hoodie"
[clothes18]: https://www.rei.com/product/856652/rei-ultralight-merino-wool-hiking-low-socks "REI merino wool ultra-light hiking socks"
[clothes19]: http://amzn.to/2btQZg6 "Columbia Silver Ridge short sleeve button-up shirt"

[shoes1]: http://amzn.to/2bLbYIK "OluKai Hiapo flip flops"
[shoes2]: http://amzn.to/2bTgFUw "Ecco BIOM Grip shoes"

[acc1]: http://www.jetpens.com/Nomadic-PE-18-Pen-Cases/ct/3480 "Nomadic PE-18 pen case"
[acc2]: http://amzn.to/2beK3Ts "Sandisk Extreme 64 GB SD card"
[acc3]: http://amzn.to/2bE7RkI "Kingston Digital MobileLite G4 USB 3.0 Multi-Function Card Reader"
[acc4]: http://amzn.to/2btTsXT "Ceptics GP-5PK International Travel Worldwide Plug Adapter Set"
[acc5]: http://www.jetpens.com/Raymay-Pencut-Mini-Scissors-Orange/pd/9653 "Raymay Pencut Mini scissors"

[misc1]: http://amzn.to/2btS2wk "Outdoor Research ultra-light stuff sack"
[misc2]: http://prices.appleinsider.com "The best place to find deals on Macs"
[misc3]: http://amzn.to/2btSdbk "Booq Taipan Spacesuit 13-inch laptop sleeve"

[hy1]: http://amzn.to/2bTkkS8 "Tweezerman petite tweezer set"
[hy2]: https://www.rei.com/product/878300/rei-micro-shower-kit "REI micro shower kit"
[hy3]: http://amzn.to/2bA8gG2 "Pacific Shaving Company caffeinated shaving cream"
[hy4]: http://www.muji.us/store/pe-tube-50g.html "Muji PE tube - 50 gram"
[hy5]: https://www.rei.com/product/785921/sea-to-summit-trek-and-travel-laundry-wash-3-fl-oz "Sea to Summit travel laundry detergent"

[loose1]: http://amzn.to/2bLlwDB "Zolt laptop charger"
[loose2]: https://www.amazon.com/Outlets-Go-Power-Strip-USB/dp/B0018MEBNG/ref=sr_1_2?s=aht&ie=UTF8&qid=1471992047&sr=1-2&keywords=monster+on+the+go "Monster On The Go power strip"
[loose3]: https://www.etsy.com/shop/LinenStory?ref=l2-shopheader-name "Linen towels"
[loose4]: http://snarkynomad.com/the-best-travel-towels-arent-where-youll-find-them/ "Linen towels, the pitch"
[loose5]: http://amzn.to/2bdpZDS "Travel money belt"
[loose6]: http://amzn.to/2bEfAzr "Sea to Summit travel clothesline"
