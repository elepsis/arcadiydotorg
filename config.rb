###
# General settings
###

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

# Sets directories to nicer than default values
set :css_dir, 'css'
set :js_dir, 'js'
set :images_dir, 'img'

activate :google_analytics do |ga|
  ga.tracking_id = 'UA-56246735-1' # Replace with your property ID.
end

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Don't lay out xml, json or txt
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

###
# Blog settings
###

# Time.zone = "UTC"

activate :blog do |blog|
  # This will add a prefix to all links, template references and source paths
  # blog.prefix = "blog"

  blog.permalink = "{year}/{month}/{title}.html"
  # Matcher for blog source files
  blog.sources = "/content/{year}-{month}-{title}.html"
  # blog.taglink = "tags/{tag}.html"
  blog.layout = "article_layout"
  blog.summary_separator = /(READMORE)/
  blog.summary_length = 500
  # blog.year_link = "{year}.html"
  # blog.month_link = "{year}/{month}.html"
  # blog.day_link = "{year}/{month}/{day}.html"
  # blog.default_extension = ".md.erb"

  # Category pages
  blog.custom_collections = {
    category: {
      link: '/categories/{category}.html',
      template: '/category.html'
    }
  }


  blog.tag_template = "tag.html"
  blog.calendar_template = "calendar.html"

  # Enable pagination
  blog.paginate = true
  blog.per_page = 6
  blog.page_link = "page/{num}"
end

# Makes for pretty URLs that don't end with .html
activate :directory_indexes

# Adds sizes to image tags based on actual image size
activate :automatic_image_sizes

###
# Helpers
###

# Used for generating absolute URLs
# Makes images work on both development and production

helpers do

  def host_with_port
    [config[:host], optional_port].compact.join(':')
  end

  def optional_port
    config[:port] unless config[:port].to_i == 80
  end

  def image_url(source)
    config[:protocol] + host_with_port + image_path(source)
  end

  def root_url
  	config[:protocol] + host_with_port
  end
  
end

activate :external_pipeline,
  name: :sass,
  command: "cd source/css && sass main.css.scss main.css",
  source: "source/css",
  latency: 2

###
# Development-specific configuration
###

configure :development do
  # Used for generating absolute URLs
  set :protocol, 'http://'
  set :host, 'localhost'
  set :port, '4567'
  
  # Turns on middleman-disqus extension.
  activate :disqus do |d|
    d.shortname = "arcadiytest"
  end  
end

###
# Build-specific configuration
###

configure :build do
  activate :disqus do |d|
    d.shortname = "arcadiy"
  end

  set :protocol, 'http://'
  set :host, 'arcadiy.org'
  set :port, '80'
end


# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end
